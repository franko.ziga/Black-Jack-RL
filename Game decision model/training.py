import sys
sys.path.append('../')

from GameDecisionModel import GameDecision
from Utility.Simulation import create_one_hot_encoder, runner
from Utility.FolderManagement import save

import Game.BJGame as bj

import pandas as pd

# GAME SPECIFICATIONS
n_slots = 1
n_decks = 6
n_epochs = 5
n_max_games = 1000
money = n_max_games

game = bj.BlackJack()
game.make(n_decks, start_money=money, num_slots=n_slots, normalizator=n_max_games, render=False)
# resetting player, dealer and deck
player = GameDecision(gamma=0.8, ot_gamma=0.0, positive_reward_addition=0.47, learning_rate=1e-05)
card_encoder = create_one_hot_encoder()

# Model description
# print( player.model.summary())
# plot_model(player.model, to_file='model.png', show_shapes=True)

player, end_money, action_distribution = runner(
    game,
    player,
    card_encoder,
    n_epochs=n_epochs,
    n_max_games=n_max_games,
    traning=True,
    sleep=False
)

# player.save_model('best-gd-model.h5')
save( action_distribution, 'Testing/', 'best training action distribution.pkl')

end_money = pd.DataFrame(end_money)
end_money.to_csv('Testing/best training results.csv')

