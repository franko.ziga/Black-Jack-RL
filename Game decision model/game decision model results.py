import os
import pickle

import pandas as pd

import matplotlib.pyplot as plt


def load_model_traning_results( model_folder):
    with open(model_folder + 'training results.pkl', 'rb') as file:
        return pickle.load(file)

def load_model_testing_results( model_folder):
    with open(model_folder + 'testing results.pkl', 'rb') as file:
        return pickle.load(file)

def load_model_parameters( model_folder):
    with open(model_folder + 'gd.parameters.pkl', 'rb') as file:
        return pickle.load(file)

def load_models(base_folder='Models/'):
    model_folders = [folder for folder in os.listdir( base_folder)
                     if os.path.isdir(base_folder + folder)]

    traning_data = {model: load_model_traning_results( base_folder + model + '/') for model in model_folders}
    testing_data = {model: load_model_testing_results( base_folder + model + '/') for model in model_folders if os.path.exists( base_folder + model + '/testing results.pkl')}
    parameters = {model: load_model_parameters( base_folder + model + '/') for model in model_folders}
    return pd.DataFrame( traning_data), pd.DataFrame( testing_data), parameters

#plt.figure()
training_data, testing_data, parameters = load_models()


models = [(model, testing_data[model]) for model in testing_data.columns]
models = sorted(models, key=lambda x: -x[1].iloc[-1])[:10]


plt.xlabel('Epoch')
plt.ylabel('Money after game')
for model, results in models:
    # plt.plot(results, label=model)
    results = results.rolling(window=15, min_periods=1).mean()
    # results = results.mean()
    plt.plot(results, label=model)

plt.legend(loc='upper left')

print('There is {} tested models'.format( len( models)))
print()
print('10 best models:')
for model, score in models[:10]:
    print('\t{} :{}     -> par: {}, gamma: {}, ot gamma: {}, lr: {}'.format(model, round(score), round(parameters[model]['positive additional reward'], 2), parameters[model]['gamma'], parameters[model]['ot gamma'], parameters[model]['learning rate']))

plt.show()