import sys
sys.path.append('../')

from GameDecisionModel import GameDecision
from Utility.Simulation import create_one_hot_encoder, runner
from Utility.FolderManagement import save, prepare_folder

import Game.BJGame as bj

import numpy as np


# HYPERPARAMETER SEARCH
def create_random_game_decision_parameters():
    model_parameters = {}
    model_parameters[ 'learning rate'] = np.random.choice([0.001, 0.0005, 0.00025, 0.0001, 0.00005, 0.000025, 0.00001]) # 10 ** (-np.random.uniform(1.3, 3))
    model_parameters[ 'gamma'] = np.random.choice([0.8, 0.9, 0.99, 0.999, 1.]) # np.random.uniform(0.99000, 0.99999)
    model_parameters[ 'ot gamma'] = np.random.choice([0., 0.25, 0.4, 0.5, 0.75, 0.8, 0.85, 0.9, 0.99, 0.999])
    model_parameters[ 'positive additional reward'] = np.random.uniform(-.5, .5)
    return model_parameters

def display_secifications(parameters):
    print('Game decision parameters')
    for key in parameters.keys():
        print('{} -> {}'.format(key, parameters[key]))

def create_model(parameters):
    model = GameDecision(
        learning_rate=parameters['learning rate'],
        gamma=parameters['gamma'],
        ot_gamma=parameters['ot gamma'],
        positive_reward_addition=parameters['positive additional reward']
    )
    return model

def random_hyperparameter_search(n_models, n_slots=1, n_decks=6, n_epochs=3, n_max_games=1000, money=1000, base_folder='Models/'):
    for mdl in range( n_models):
        game = bj.BlackJack()
        game.make(n_decks, start_money=money, num_slots=n_slots)

        # creating new model
        model_parameters = create_random_game_decision_parameters()
        player = create_model( model_parameters)
        card_encoder = create_one_hot_encoder()

        print('training model: ', mdl)
        display_secifications( model_parameters)

        player, training_results, training_action_distribution = runner(game, player, card_encoder, n_epochs=n_epochs, n_max_games=n_max_games)
        print('testing model: ', mdl)
        _, testing_results, testing_action_distribution = runner(game, player, card_encoder, n_epochs=n_epochs, n_max_games=n_max_games, traning=False)

        # saving model, model parameters and results
        path = prepare_folder( base_folder)
        save(training_results, path, 'training results.pkl')
        save(training_action_distribution, path, 'training action distribution.pkl')
        save(model_parameters, path, 'testing results.pkl')
        save(model_parameters, path, 'testing action distribution.pkl')
        player.save_model(path + 'game.decision.h5')

random_hyperparameter_search(1)