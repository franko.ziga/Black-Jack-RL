from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU, PReLU
from keras.optimizers import Adam

import random
import numpy as np
from collections import deque

class GameDecision:
    def __init__(self, learning_rate=0.0001, lr_decay=0.000832, memory_length=20000, min_memory_length=3000, batch_size=32,
                 gamma=1., ot_gamma=0.99, start_epsilon=1., min_epsilon=0.1, epsilon_decay=0.99994,
                 clipping_value=1.7, positive_reward_addition=0, dropout_rate=0.1):
        self.n_inputs = 118
        self.n_actions = 5

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size

        self.ot_gammma = ot_gamma
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, lr_decay, clipping_value)

        self.min_memory_length = min_memory_length
        self.memory = self._create_replay_memory( memory_length)
        self.game_memory = []

    def _build_netowrk(self, learning_rate, lr_decay, clipping_value):
        model = Sequential()
        model.add(Dense(256, kernel_initializer='glorot_normal', activation='linear', input_dim=self.n_inputs, name='Hidden-layer-1'))
        model.add(PReLU(name='activation-1'))
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add(Dense(128, kernel_initializer='glorot_normal', activation='linear', name='Hidden-layer-2'))
        model.add(PReLU(name='activation-2'))
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add(Dense(self.n_actions, kernel_initializer='glorot_normal', activation='linear', name='Output-layer'))
        model.compile(loss='mse', optimizer=Adam(lr=learning_rate, clipvalue=0.1))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 5
        elif hand.shape[0] == 2:
            return 4
        return 2

    def _epsilon_decay_step(self):
        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        possibilities = [1, 1, 1, 1, 3][:max_action]

        if np.random.rand() < self.epsilon:
            return np.random.choice([0, 1, 2, 3, 4][:max_action], p=possibilities/np.sum(possibilities))

        model_action = self.model.predict( x)[0][ :max_action]
        return np.argmax( model_action)

    def train(self):
        if self.min_memory_length > len( self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done in mini_batch:
            if done:
                target = reward
            else:
                target = reward + np.sum( [self.gamma * np.amax( self.model.predict( sub_next_state)[0])
                                           for sub_next_state in next_state])
            y = self.model.predict( state)
            y[0][action] = target

            y_mini_batch.append( y)
            x_mini_batch.append( state)

        x_mini_batch = np.array( x_mini_batch).reshape( [self.batch_size, self.n_inputs])
        y_mini_batch = np.array( y_mini_batch).reshape( [self.batch_size, self.n_actions])

        loss = self.model.fit( x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean( loss.history[ 'loss'])

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done, next_slot):
        self.game_memory.append( (state, action, reward, [next_state], done, next_slot))

    def end_game_repare(self):
        prev_reward = 0

        for index in reversed( range( len( self.game_memory))):
            s, a, r, n_s, done, nx_s = self.game_memory[ index]
            if done:
                r = r + prev_reward * self.ot_gammma

            prev_reward = r
            self.memory.append( (s, a, r, n_s, done))

    def repare_rewards(self, end_rewards):
        last_index = len( end_rewards)-1
        memory_index = len( self.game_memory)-1

        end_rewards = np.array( end_rewards, dtype=np.float32)

        if self.positive_reward_addition < 0:
            end_rewards[ end_rewards < 0.] -= self.positive_reward_addition
        else:
            end_rewards[ end_rewards > 0.] += self.positive_reward_addition

        sub_slot_state = None
        while last_index >= 0:
            state, action, reward, next_state, done, new_slot = self.game_memory[ memory_index]
            if action == 3:
                self.game_memory[ memory_index] = (state, action, reward, [next_state[0], sub_slot_state], done, new_slot)
            if new_slot:
                sub_slot_state = state
            if done:
                reward = end_rewards[ last_index]
                self.game_memory[ memory_index] = (state, action, reward, next_state, done, new_slot)
                last_index -= 1
            memory_index -= 1

test = GameDecision()