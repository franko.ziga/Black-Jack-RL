import sys
sys.path.append('../')

from GameDecisionModel import GameDecision
from Utility.Simulation import create_one_hot_encoder, runner
from Utility.FolderManagement import save

import Game.BJGame as bj

import pandas as pd


# Game specifications
n_slots = 1
n_decks = 6
n_epochs = 5
n_max_games = 1000
money = n_max_games

# making a new Black-jack game
game = bj.BlackJack()
game.make(n_decks, start_money=money, num_slots=n_slots, normalizator=n_max_games, render=False)
# resetting player, dealer and deck
player = GameDecision(start_epsilon=0)
player.load_model( 'best-gd-model.h5')
card_encoder = create_one_hot_encoder()

#
player, end_money, action_distribution = runner(
    game,
    player,
    card_encoder,
    n_epochs=n_epochs,
    n_max_games=n_max_games,
    traning=False,
    sleep=False
)

save( action_distribution, 'Testing/', 'testing action distribution.pkl')

end_money = pd.DataFrame(end_money)
end_money.to_csv('Testing/testing results.csv')

