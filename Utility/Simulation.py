import time
import numpy as np
from sklearn.preprocessing import OneHotEncoder


def create_one_hot_encoder():
    card_encoder = OneHotEncoder()
    values = [[val] for val in range(1, 14, 1)]
    card_encoder.fit( values)
    return card_encoder

def convert_inputs( state, card_encoder, n_card_slots=8):
    x = card_encoder.transform( [state[ 'Dealer']]).toarray() # 13 inputs

    n_current_slot = len( state['Current slot'])
    hand = [[card*13] for card in state[ 'Current slot']]

    encoded_hand = card_encoder.transform( hand).toarray()
    if n_current_slot < n_card_slots:
        zeros = np.zeros((n_card_slots - n_current_slot, 13))
        encoded_hand = np.concatenate([encoded_hand, zeros])

    else:
        encoded_hand = encoded_hand[:n_card_slots, :]

    x = np.concatenate([x, encoded_hand]).flatten() # 13 * 8 = 104
    x = np.append(x, state['Deck value'])
    return np.reshape(x, [1, len(x)]) # 104 + 13 + 1 = 118


def betting_step(game):
    # resetting all cards and bets
    obs = game.reset()

    if obs['Current money'][-1] <= 0:
        return

    # bets = player.action_bet( obs)
    bets = [1]
    _ = game.set_bets(bets)

def game_decision_step(game, player, card_encoder, sleep=False):
    game.deal_cards()

    state, reward, done = game.observe_game()
    if 'Current slot' not in state:
        return None

    slot_index = state['Slot index']
    hand = state['Current slot']
    state = convert_inputs(state, card_encoder)

    actions=[]

    while True:
        if sleep: time.sleep(2)
        action = player.action(state, hand)
        actions.append(action)
        game.game_step(action)

        next_state, reward, done = game.observe_game()

        if done:
            player.remember(state, action, 0, None, True, (next_state['Slot index'] != slot_index))
            break
        elif next_state['Slot index'] != slot_index:
            player.remember(state, action, 0, None, True, True)
            hand = next_state['Current slot']
            slot_index = next_state['Slot index']
            next_state = convert_inputs(next_state, card_encoder)
        else:
            hand = next_state['Current slot']
            next_state = convert_inputs(next_state, card_encoder)
            player.remember(state, action, 0, next_state, False, False)

        state = next_state

    player.repare_rewards(reward)
    return actions

def runner(game, player, card_encoder, n_epochs=150, n_max_games=831, sub_print=True, traning=True, sleep=False):
    end_money = {}
    action_distribution = []

    for epoch in range(n_epochs):
        money = []
        game.reset_iteration()
        actions = []

        for game_index in range(n_max_games):
            betting_step(game)
            sub_actions = game_decision_step(game, player, card_encoder, sleep=sleep)

            if sub_actions == None:
                break

            actions.extend( sub_actions)

            if traning:
                loss = player.train()
                if (game_index % 250 == 0) and sub_print:
                    print('game index: {}, epsilon: {}, loss: {}'.format(game_index, player.epsilon, loss))
            money.append( game.money)

            if sleep: time.sleep(3)

        player.end_game_repare()
        end_money['Game ' + str( epoch)] = money
        action_distribution.append({['Stand', 'Hit', 'Double', 'Surrender', 'Split'][index]: actions.count(index)
                                    for index in range(5)})
        if sub_print:
            print('epoch: {}, end game index: {}, end game money: {}'.format(epoch, game_index, game.money))
            print('action distribution stand: {}, hit: {}, double: {}, split: {}, surrender: {}'.format(
                actions.count(0),
                actions.count(1),
                actions.count(2),
                actions.count(4),
                actions.count(3)
            ))
            print()

    return player, end_money, action_distribution