import os
import pickle

def make_directory(folder_path):
    if not os.path.exists( folder_path):
        os.makedirs( folder_path, exist_ok=True)

def next_directory(folder_base):
    models = [int(model_name.split(' ')[1]) for model_name in os.listdir( folder_base)
              if os.path.isdir(folder_base + model_name)]
    if len( models) == 0:
        model_name = 'Model 0'
    else:
        last_number = int( sorted( models)[-1]) + 1
        model_name = 'Model ' + str( last_number)
    path = folder_base + model_name + '/'
    make_directory(path)
    return path

def file_check(folder, file):
    assert os.path.exists( folder + file), 'file: {} does not exist!'.format(folder + file)


# USED FUNCTIONS

def save(object, folder, file):
    make_directory( folder)
    with open(folder + file, 'wb') as f:
        pickle.dump(object, f)

def load(folder, file):
    file_check(folder, file)
    with open(folder + file, 'rb') as f:
        return pickle.load( f)

def prepare_folder(folder_path):
    make_directory( folder_path)
    new_folder_path = next_directory( folder_path)
    return new_folder_path
