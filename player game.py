import time
import Game.BJGame as bj
import Game.BJPeople as bjp

def insert_bet(n_slots):
    bets = [0.] * n_slots

    index = 0
    while index < n_slots:
        try:
            bets[ index] = float( input( 'Insert bet for slot {}: '.format( index+1)))
        except:
            index -= 1
        index += 1
    return bets

if __name__ == '__main__':

    player = bjp.Player("Player")
    n_slots = 3

    game = bj.BlackJack()
    game.make( 8, num_slots=n_slots, render=True)
    # resetting player, dealer and deck
    game.reset_iteration()

    while True:
        # resetting all cards and bets
        obs = game.reset()

        if obs['Current money'][-1] <= 0:
            break

        # bets = player.action_bet( obs)
        print('Money: {}'.format( obs['Current money'][-1]))
        print('Bet history:\n', obs[ 'Bet history'][-5:])
        bets = insert_bet( n_slots)
        done = game.set_bets( bets)
        if done:
            break

        game.deal_cards ()

        time.sleep(0.5)
        obs, reward, done = game.observe_game()
        print('Cards distribution: {}\nDealers card: {}\nCurrent cards: {}'.format( obs['Card history'][-1], obs['Dealer'], obs['Current slot']))

        while True:
            action = player.action( obs)
            game.game_step( action)

            obs, reward, done = game.observe_game()
            if done:
                print(game.display())
                print('Rewards: ', reward)
                break
            else:
                print('Cards distribution: {}\nDealers card: {}\nCurrent cards: {}'.format( obs['Card history'][-1], obs['Dealer'], obs['Current slot']))
        input('press enter!')
        print('\n')