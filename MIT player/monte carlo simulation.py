import pickle

import matplotlib.pyplot as plt

# with open('bets.pkl', 'rb') as file:
#    bets = pickle.load( file)
#with open('pct.pkl', 'rb') as file:
#    pct = pickle.load( file)

with open('Base lines/winning rate.pkl', 'rb') as file:
    pct = pickle.load( file)

# with open('bets_overall.pkl', 'rb') as file:
#    bets_overall = pickle.load( file)

bets = sorted([int(key) for key in pct.keys() if (int(key) >= -15) and  int(key) <= 15])
pct = [float(pct[key]) for key in bets]
plt.figure()
# plt.plot([-15, 15], [.5, .5], '--', color='orange')
plt.bar( bets, pct)
plt.xticks( bets, [int(b) for b in bets], rotation=60)
plt.ylim(0.45, 0.55)
plt.title('Winning rate at a certain true count')
plt.xlabel('True count')
plt.ylabel('Winning rate')
plt.grid(True, linestyle='-', linewidth=.3)

plt.show()