import numpy as np
import Game.BJCards as bjc

from sklearn.preprocessing import OneHotEncoder

import math
import matplotlib.pyplot as plt

def plot_distribution( distribution, decimals=2):
    rounded_distribution = np.round( distribution, decimals=decimals)
    multiplier = 10 ** decimals

    min = np.min( rounded_distribution) * multiplier
    ln = int(min * -1 + np.max( rounded_distribution) * multiplier)

    x, y = np.zeros((1, ln)), np.zeros((1, ln))
    for num in rounded_distribution:
        index = int(num + min)
        x[0, index] = num
        y[0, index] += 1
    return x, y

def exponential_fun(gama=1):
    random = np.random.rand(10000)
    return gama * np.e **(-gama * random)

''' TESTING DISTRIBUTION'''

mean, std = 1, 2

# distribution = np.random.normal(mean, std, 10000)
#random = np.random.rand(10000)
#distribution = 5 ** random

#distribution = exponential_fun()
#distribution = sorted( distribution)
# distribution = np.random.exponential(1, size=10000)


'''
x, y = plot_distribution( distribution)
x, y = x[0], y[0]

print( 'lenght distribution: {}'.format( len( distribution)))
print( 'max x: {}, min x: {}'.format(np.max( x), np.min( x)))
print( 'max y: {}, min y: {}'.format(np.max( y), np.min( y)))
print( 'max distribution: {}, min distribution: {}'.format(np.max( distribution), np.min( distribution)))

# plt.plot( distribution)

plt.plot(x, y)
plt.show()
'''

''' REPARING BETS'''
def repare_bets(slot_bets, money):
    mean_difference = np.sum(slot_bets) - money
    if mean_difference <= 0:
        return slot_bets

    for index in range(len(slot_bets)):
        if slot_bets[index] > 0:
            slot_bets[index] -= mean_difference
            mean_difference = 0
        if slot_bets[index] < 0:
            mean_difference = slot_bets[index] * -1
            slot_bets[index] = 0
        if mean_difference == 0:
            break

    return slot_bets
'''
input = np.array([1., 12.0, 9.0])
#input.astype( np.float32)
print(repare_bets(input, 14))
'''


''' TESTING DECAY '''
'''
epochs = np.array( list( range( 100)))

decay=0.95

learning_rate_start = 0.7

learning_rate1 = learning_rate_start / (1 + 0.1 * epochs)
learning_rate2 = (decay ** epochs) * learning_rate_start

print( 'minumun learning rate 1:', np.min( learning_rate1))
print( 'minumun learning rate 2:', np.min( learning_rate2))

plt.title( 'Decays')
plt.plot( learning_rate1)
plt.plot( learning_rate2)
plt.show()
'''

'''TESTING CARD COUNTING'''

def draw_card( deck, n_decks=8):
    restore = False
    if deck.cardsSize() <= 2:
        restore = True
        deck = bjc.Deck( n_decks)

    return deck.popCard().getValue(), restore

'''
deck_distributions = {}
for deck_index in range(1, 20, 2):
    distribution = dict()
    for _ in range( 10):
        for game in range( 20):
            sum = 0
            deck = bjc.Deck( deck_index)
            for _ in range(100):
                card_value, reset = draw_card(deck, n_decks=deck_index)

                if card_value < 7:
                    card_value = 1
                elif card_value > 9:
                    card_value = -1
                else:
                    card_value = 0

                if reset:
                    sum = 0

                sum += card_value

                distribution[sum] = distribution.setdefault(sum, 0) + 1
    deck_distributions[ str(deck_index)] = distribution

plt.figure()

for key in deck_distributions:
    x = sorted( deck_distributions[key].keys())
    y = [deck_distributions[key][ index] for index in x]
    plt.plot(x, y, label='deck len ' + key)

plt.legend(loc='best')
plt.show()

# print(distribution)
'''

'''
TESTING true count

n_decks = 4

deck_distribution = {}
tcount = []
for n_decks in range(4, 5, 1):

    history = {}
    for _ in range(100):
        deck = bjc.Deck(n_decks)

        running_count = 0
        drawn_cards = 0
        for _ in range(300):
            for _ in range(9):
                card_value, restore = draw_card(deck, n_decks=n_decks)

                if restore:
                    running_count = 0
                    drawn_cards = 0

                if card_value <= 6:
                    running_count += -1
                elif card_value >= 10:
                    running_count += 1

                drawn_cards += 1

            current_deck_length = math.ceil( (n_decks * 52 - drawn_cards)/52)

            true_count = round(running_count / current_deck_length)
            tcount.append( true_count)
            history[ true_count] = history.setdefault(true_count, 0) + 1

    deck_distribution[ n_decks] = history


plt.figure()

plt.title( 'Number of true count values on logarithmic scale')
plt.xlabel( 'True count value')
plt.ylabel( 'Number of true counts')

for deck_size in deck_distribution.keys():
    history = deck_distribution[ deck_size]

    x = sorted( history.keys())
    y = np.log([history[ key] for key in x])

    plt.plot(x, y, label=str( deck_size))

plt.legend(loc='best')

split = -2

tcount = np.array( tcount)
tclen = len( tcount)
print( 'after {} game, low bet: {}%, high bet: {}%'.format(tclen, np.sum( tcount < split) / tclen * 100, np.sum( tcount >= split)/tclen * 100))
print( 'low bet: {}, high bet: {}'.format(np.sum( tcount < split), np.sum( tcount >= split)))
plt.show()
'''

''' 

CARD ENCODE TEST

deck = bjc.Deck(n_decks=8)

person1 = bjc.Deck(n_decks=0)
person1.addCard( draw_card(deck, n_decks=8)[0])
person1.addCard( draw_card(deck, n_decks=8)[0])
person1.addCard( draw_card(deck, n_decks=8)[0])

print( person1.encodedDeck())
'''


'''
REPARING BETS
'''
def repare_bets(slot_bets, money):
    bets = np.copy( slot_bets)
    difference = np.sum(bets) - money
    if difference <= 0:
        return bets

    for index in range(len(bets)):
        if bets[index] > 0:
            bets[index] -= difference
            difference = 0
        if bets[index] < 0:
            difference += bets[index] * -1
            bets[index] = 0
        if difference == 0:
            break

    return bets

'''
bets=[-1, 15, 32]
money=1
result=repare_bets(bets, money)
print( 'bets= {}, money= {} ;; result= {}'.format(bets, money, result))
'''


'''One hot encoder'''
def create_one_hot_encoder():
    card_encoder = OneHotEncoder()
    values = [[val] for val in range(1, 14, 1)]
    card_encoder.fit( values)
    return card_encoder

enc = create_one_hot_encoder()
print( enc.transform( [[5]]).toarray())