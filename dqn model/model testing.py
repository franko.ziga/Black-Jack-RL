import sys
sys.path.append('../')

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU, PReLU
from keras.optimizers import Adam

from sklearn.preprocessing import OneHotEncoder

import os
import random
import pickle
import numpy as np
import pandas as pd
from collections import deque

import Game.BJGame as bj

'''
class GameDecision:
    def __init__(self, learning_rate=0.005, lr_decay=0.01, memory_length=16384, batch_size=32,
                 gamma=0.99, start_epsilon=.8, min_epsilon=0.1, epsilon_decay=0.9997,
                 clipping_value=1., positive_reward_addition=0.3, dropout_rate=0.1):
        self.n_inputs = 11
        self.n_actions = 5

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, lr_decay, clipping_value)
        self.memory = self._create_replay_memory( memory_length)

    def _build_netowrk(self, learning_rate, lr_decay, clipping_value):
        model = Sequential()
        model.add( Dense( 128, activation='linear', input_dim=self.n_inputs))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 64, activation='linear'))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( self.n_actions, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=learning_rate, clipvalue=clipping_value, decay=lr_decay))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 5
        elif hand.shape[0] == 2:
            return 4
        return 2

    def _epsilon_decay_step(self):
        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        possibilities = [1, 1, 1, 1, 3][:max_action]

        if np.random.rand() < self.epsilon:
            return np.random.choice([0, 1, 2, 4, 3][:max_action], p=possibilities/np.sum(possibilities))

        model_action = self.model.predict( x)[0]
        return np.argmax( model_action)

    def train(self):
        if self.batch_size > len( self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done, _ in mini_batch:
            if done:
                target = reward
            else:
                target = reward + np.sum( [self.gamma * np.amax( self.model.predict( sub_next_state)[0])
                                           for sub_next_state in next_state])
            y = self.model.predict( state)
            y[0][action] = target

            y_mini_batch.append( y)
            x_mini_batch.append( state)

        x_mini_batch = np.array( x_mini_batch).reshape( [self.batch_size, self.n_inputs])
        y_mini_batch = np.array( y_mini_batch).reshape( [self.batch_size, self.n_actions])

        loss = self.model.fit( x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean( loss.history[ 'loss'])

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done, next_slot):
        self.memory.append( (state, action, reward, [next_state], done, next_slot))

    def repare_rewards(self, end_rewards):
        last_index = len( end_rewards)-1
        memory_index = len( self.memory)-1

        end_rewards = np.array( end_rewards, dtype=np.float32)
        if self.positive_reward_addition < 0:
            end_rewards[ end_rewards < 0.] += self.positive_reward_addition
        else:
            end_rewards[ end_rewards > 0.] += self.positive_reward_addition

        sub_slot_state = None
        while last_index >= 0:
            state, action, reward, next_state, done, new_slot = self.memory[ memory_index]

            if action == 3:
                self.memory[ memory_index] = (state, action, reward, [next_state[0], sub_slot_state], done, new_slot)
            if new_slot:
                sub_slot_state = state
            if done:
                reward = end_rewards[ last_index]
                self.memory[ memory_index] = (state, action, reward, next_state, done, new_slot)
                last_index -= 1

            memory_index -= 1


def convert_inputs( state, n_card_slots=8):
    x = []
    x.append( state['Current bet']) # 1 position
    x.extend( state['Dealer']) # 1 position
    x.append( state['Deck value'])

    n_current_slot = len( state['Current slot'])
    if n_current_slot <= n_card_slots:
        difference = [0] * (n_card_slots - n_current_slot)
        slot_cards = np.concatenate([state['Current slot'], difference])
    else:
        slot_cards = state['Current slot'][:n_card_slots]

    x.extend( slot_cards) # n card slots
    x = np.array( x)
    x = np.reshape(x, [1, len(x)]) # all of them forms size of 24 inputs, currently used only 23!
    return x
'''
class GameDecision:
    def __init__(self, learning_rate=0.007, lr_decay=0.000832, memory_length=4000, batch_size=32,
                 gamma=0.99, start_epsilon=.8, min_epsilon=0.1, epsilon_decay=0.9998,
                 clipping_value=1.7, positive_reward_addition=0., dropout_rate=0.1):
        self.n_inputs = 117
        self.n_actions = 5

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, lr_decay, clipping_value)
        self.memory = self._create_replay_memory( memory_length)

    def _build_netowrk(self, learning_rate, lr_decay, clipping_value):
        model = Sequential()
        model.add( Dense( 256, activation='linear', input_dim=self.n_inputs))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 64, activation='linear'))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( self.n_actions, activation='linear'))
        model.compile(loss='mae', optimizer=Adam(lr=learning_rate, clipvalue=clipping_value, decay=lr_decay))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 5
        elif hand.shape[0] == 2:
            return 4
        return 2

    def _epsilon_decay_step(self):
        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        possibilities = [1, 1, 1, 1, 3][:max_action]

        if np.random.rand() < self.epsilon:
            return np.random.choice([0, 1, 2, 4, 3][:max_action], p=possibilities/np.sum(possibilities))

        model_action = self.model.predict( x)[0]
        return np.argmax( model_action)

    def train(self):
        if self.batch_size > len( self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done, _ in mini_batch:
            if done:
                target = reward
            else:
                target = reward + np.sum( [self.gamma * np.amax( self.model.predict( sub_next_state)[0])
                                           for sub_next_state in next_state])
            y = self.model.predict( state)
            y[0][action] = target

            y_mini_batch.append( y)
            x_mini_batch.append( state)

        x_mini_batch = np.array( x_mini_batch).reshape( [self.batch_size, self.n_inputs])
        y_mini_batch = np.array( y_mini_batch).reshape( [self.batch_size, self.n_actions])

        loss = self.model.fit( x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean( loss.history[ 'loss'])

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done, next_slot):
        self.memory.append( (state, action, reward, [next_state], done, next_slot))

    def repare_rewards(self, end_rewards):
        last_index = len( end_rewards)-1
        memory_index = len( self.memory)-1

        end_rewards = np.array( end_rewards, dtype=np.float32)
        if self.positive_reward_addition < 0:
            end_rewards[ end_rewards < 0.] += self.positive_reward_addition
        else:
            end_rewards[ end_rewards > 0.] += self.positive_reward_addition

        sub_slot_state = None
        while last_index >= 0:
            state, action, reward, next_state, done, new_slot = self.memory[ memory_index]
            if action == 3:
                self.memory[ memory_index] = (state, action, reward, [next_state[0], sub_slot_state], done, new_slot)
            if new_slot:
                sub_slot_state = state
            if done:
                reward = end_rewards[ last_index]
                self.memory[ memory_index] = (state, action, reward, next_state, done, new_slot)
                last_index -= 1
            memory_index -= 1


def convert_inputs( state, n_card_slots=8):
    x = card_encoder.transform( [state[ 'Dealer']]).toarray() # 13 inputs
    # x.append( state['Current bet']) # 1 position
    # x.extend( card_encoder.transform( state['Dealer'])) # 13 position
    # x.append( state['Deck value'])

    n_current_slot = len( state['Current slot'])
    hand = [[card*13] for card in state[ 'Current slot']]

    encoded_hand = card_encoder.transform( hand).toarray()
    if n_current_slot < n_card_slots:
        zeros = np.zeros((n_card_slots - n_current_slot, 13))
        encoded_hand = np.concatenate([encoded_hand, zeros])

    else:
        encoded_hand = encoded_hand[:n_card_slots, :]

    x = np.concatenate([x, encoded_hand]).flatten() # 13 * 8 = 104
    # print( x.flatten())
    # print(x)
    return np.reshape(x, [1, len(x)]) # 104 + 13 = 117

def betting_step(game):
    # resetting all cards and bets
    obs = game.reset()

    if obs['Current money'][-1] <= 0:
        return

    # bets = player.action_bet( obs)
    bets = [1, 1, 1]
    _ = game.set_bets(bets)
    return bets

def betting_step_mit(game):
    # resetting all cards and bets
    obs = game.reset()

    if obs['Current money'][-1] <= 0:
        return

    value_bet = obs[ 'Card history'][-1][0] * (-1) - 1
    if value_bet < 0:
        value_bet = 0
    elif value_bet > 6:
        value_bet = 6

    bet = value_bet + 1
    bets = [bet, bet, bet]
    _ = game.set_bets(bets)
    return bets

def game_decision_step(game, player):
    game.deal_cards()

    state, reward, done = game.observe_game()
    slot_index = state['Slot index']
    hand = state['Current slot']
    state = convert_inputs(state)

    actions=[]

    while True:
        action = player.action(state, hand)
        actions.append(action)
        game.game_step(action)

        next_state, reward, done = game.observe_game()

        if done:
            player.remember(state, action, 0, None, True, (next_state['Slot index'] != slot_index))
            break
        elif next_state['Slot index'] != slot_index:
            player.remember(state, action, 0, None, True, True)
            hand = next_state['Current slot']
            slot_index = next_state['Slot index']
            next_state = convert_inputs(next_state)
        else:
            hand = next_state['Current slot']
            next_state = convert_inputs(next_state)
            player.remember(state, action, 0, next_state, False, False)

        state = next_state

    player.repare_rewards(reward)
    return actions

def testing(game, player, n_epochs=150, n_max_games=831):
    data = {}
    for epoch in range(n_epochs):
        if epoch % 1 == 0:
            print('\tepoch: ', epoch)
        game.reset_iteration()
        money = []

        for game_index in range(n_max_games):
            betting_step(game)
            _ = game_decision_step(game, player)
            money.append( game.money)

        data['Game ' + str( epoch)] = money

    return player, pd.DataFrame( data)

def create_model(parameters):
    model = GameDecision(
        learning_rate=parameters['learning rate'],
        lr_decay=parameters['learning rate decay'],
        gamma=parameters['gamma'],
        memory_length=parameters['memory length'],
        batch_size=parameters['batch size'],
        epsilon_decay=parameters['epsilon decay'],
        clipping_value=parameters['clipping value'],
        positive_reward_addition=parameters['positive additional reward']
    )
    return model

def load_trained_model( folder):
    model = GameDecision(start_epsilon=0., )
    model.load_model( folder + 'game-decision v2.h5')
    return model


# if normal testing use file name: testing results.csv
# else if mit beting: mit testing results.csv
def model_tester( base_folder='models/', result_file='mit testing results.csv', n_slots=3, n_decks=8, n_epochs=10, n_games=831, start_money=831, update_models=False):
    game = bj.BlackJack()
    game.make(num_decks=n_decks, num_slots=n_slots, start_money=start_money)

    for model_name in os.listdir( base_folder):
        model_folder = base_folder + model_name + '/'
        if not os.path.isdir( model_folder): continue
        if os.path.exists(model_folder + result_file) and (not update_models): continue
        print('testing model: {}...'.format( model_name))
        model = load_trained_model( model_folder)

        _, testing_results = testing(game, model, n_epochs=n_epochs, n_max_games=n_games)
        testing_results.to_csv( model_folder + result_file)

# model_tester(n_epochs=10, update_models=False)

def create_one_hot_encoder():
    card_encoder = OneHotEncoder()
    values = [[val] for val in range(1, 14, 1)]
    card_encoder.fit( values)
    return card_encoder

card_encoder = create_one_hot_encoder()

model = load_trained_model('')
game = bj.BlackJack()
game.make(num_decks=6, num_slots=3, start_money=1000)
_, results = testing(game, model, n_epochs=20, n_max_games=1000)
results.to_csv('current model results v3.csv')
