import pandas as pd
import numpy as np
import pickle
import os

import matplotlib.pyplot as plt

def load_traning_rewards(folder):
    with open( folder + 'traning results.pkl', 'rb') as file:
        traning_results = pickle.load( file)
    with open( folder + 'gd.parameters.pkl', 'rb') as file:
        model_parameters = pickle.load( file)
    return traning_results, model_parameters

def load_models_traning_result( folder='models/'):
    data = pd.DataFrame()
    parameter_data = pd.DataFrame()

    for sub_folder in os.listdir( folder):
        if not os.path.isdir(folder + sub_folder): continue
        data[ sub_folder], model_parameters = load_traning_rewards( folder + sub_folder + '/')

        model_parameters[ 'index'] = sub_folder

        model_data = pd.DataFrame( model_parameters, index=[0])
        if parameter_data.empty:
            parameter_data = model_data
        else:
            parameter_data = parameter_data.append( model_parameters, ignore_index=True)

    return data, parameter_data

def load_model_testing_results( folder='models/', result_file='testing results.csv'):
    data = pd.DataFrame()

    for model_name in os.listdir( folder):
        if not os.path.isdir(folder + model_name + '/'): continue

        test_result_file = folder + model_name + '/' + result_file
        if not os.path.exists( test_result_file ): continue

        model_data = pd.read_csv( test_result_file )
        model_data.rename( columns={key: '{} {}'.format(model_name, key) for key in model_data.columns}, inplace=True)

        if data.empty:
            data = model_data
        else:
            data = pd.concat([data, model_data], axis=1)

    data = clean_data( data)
    data = model_mean( data)
    return data

def clean_data( data):
    drop = [column for column in data.columns if 'Unnamed' in column]
    data.drop(drop, axis=1, inplace=True)
    return data

def model_mean( data):
    mean_columns = dict()
    for column in data.columns:
        model_name = ' '.join(column.split(' ')[:2])
        current_list = mean_columns.get( model_name, [])
        current_list.append( column)
        mean_columns[ model_name] = current_list

    mean_data = {name: data[ columns].mean(axis=1) for name, columns in mean_columns.items()}
    return pd.DataFrame(mean_data)

def plot_testing_results( data):
    plt.figure()
    x = data.index

    for model in data.columns:
        y = data[ model].as_matrix()
        plt.plot(x, y, label=model)

    plt.legend(loc='best')

def plot_traning_results( data):
    plt.figure()
    x = data.index

    for model in data.columns:
        y = data[ model].as_matrix()
        y = pd.rolling_mean( y, window=10, min_periods=0)
        plt.plot(x, y, label=model)

    plt.legend(loc='best')

'''MODEL TESTING
traning_data, parameter_data = load_models_traning_result()

testing_data = load_model_testing_results(result_file='mit testing results.csv')

# print( parameter_data)

best_models = [(model, testing_data[ model].iloc[-1]) for model in testing_data.columns]
best_models = sorted(best_models, key=lambda model: -model[1])
print( best_models[:10])

parameter_data.set_index('index', inplace=True)
parameter_data = parameter_data.transpose()

columns = [model for model, _ in best_models[:10]]
scores = [score for _, score in best_models[:10]]
print( parameter_data[ columns])
plot_traning_results( traning_data[ columns])
plot_testing_results( testing_data[ columns])
# print( parameter_data)
plt.show()
'''

basic_player = pd.read_csv('basic player testing results 2.csv')
basic_player.drop('Unnamed: 0', axis=1, inplace=True)
basic_player = basic_player.mean(axis=1)

model_results = pd.read_csv('current model results.csv')
model_results.drop('Unnamed: 0', axis=1, inplace=True)
model_results = model_results.mean(axis=1)

model2_results = pd.read_csv('current model results v2.csv')
model2_results.drop('Unnamed: 0', axis=1, inplace=True)
model2_results = model2_results.mean(axis=1)


model3_results = pd.read_csv('current model results v3.csv')
model3_results.drop('Unnamed: 0', axis=1, inplace=True)
model3_results = model3_results.mean(axis=1)

normal_player = [1000 + (-0.04 * 3) * (index + 1) for index in range( 1000)]

plt.figure()

plt.plot(model_results.index, model_results.as_matrix(), label='model 1')
plt.plot(basic_player.index, basic_player.as_matrix(), label='encoder')
plt.plot(model2_results.index, model2_results.as_matrix(), label='model 2')
plt.plot(model2_results.index, model3_results.as_matrix(), label='model 3')
plt.plot(model2_results.index, normal_player, label='normal player')

plt.legend(loc='best')
plt.show()
