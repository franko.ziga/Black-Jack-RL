import pandas as pd
import matplotlib.pyplot as plt

training1 = pd.read_csv('traning data.csv')
training2 = pd.read_csv('traning data.2.csv')

testing1 = pd.read_csv('testing_data.csv')
testing2 = pd.read_csv('testing_data.2.csv')

training1.drop('Unnamed: 0', axis=1, inplace=True)
training2.drop('Unnamed: 0', axis=1, inplace=True)

testing1.drop('Unnamed: 0', axis=1, inplace=True)
testing2.drop('Unnamed: 0', axis=1, inplace=True)

training1 = training1.mean(axis=1)
training2 = training2.mean(axis=1)

testing1 = testing1.mean(axis=1)
testing2 = testing2.mean(axis=1)
print( training1)

plt.figure()

plt.plot(training1.index, training1.as_matrix(), label='training min ep: 0.1')
plt.plot(training2.index, training2.as_matrix(), label='training min ep: 0.05')

plt.plot(testing1.index, testing1.as_matrix(), label='testing min ep: 0.1')
plt.plot(testing2.index, testing2.as_matrix(), label='testing min ep: 0.05')

plt.legend(loc='best')

plt.show()