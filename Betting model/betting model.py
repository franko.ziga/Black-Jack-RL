import sys
sys.path.append('../')

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU, PReLU
from keras.optimizers import Adam

from keras.utils.vis_utils import plot_model

from sklearn.preprocessing import OneHotEncoder

import os
import random
import pickle
import numpy as np
import pandas as pd
from collections import deque

import Game.BJGame as bj
import Game.BJPeople as bjp


class BettingDecisionNN:

    def __init__(self, n_inputs, n_outputs, learning_rate=0.001, batch_size=32, memory_size=8000, gamma=0.999,
                 epsilon_start=1., epsilon_min=0.1, epsilon_decay=0.9997, reward_std=11.45):
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs

        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.reward_std = reward_std
        self.gamma = gamma

        self.epsilon_decay = epsilon_decay
        self.epsilon_min = epsilon_min
        self.epsilon = epsilon_start

        self.memory = deque(maxlen=memory_size)

        self.model = self._build_model()

    def _build_model(self):
        model = Sequential()

        model.add( Dense(self.n_outputs, activation='linear', input_dim=self.n_inputs, name='Output-layer'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

        return model

    def _epsilon_decay_step(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def action(self, x):
        if np.random.rand() < self.epsilon:
            return np.random.randint(0, self.n_outputs, 1)[0]

        predictions = self.model.predict( x)[0]
        return np.argmax( predictions)

    def calculate_bet(self, action, money, unit_devider=1000, unit_multiplier=[1, 5, 20, 50, 70]):
        unit = int( money / unit_devider)
        unit = unit if unit > 0 else 0
        return  unit * unit_multiplier[ action]

    def remember(self, state, action, reward, next_state, done):
        self.memory.append( (state, action, reward/self.reward_std, next_state, done))

    def train(self):
        if self.batch_size > len(self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done in mini_batch:
            if done:
                target = reward
            else:
                target = reward + self.gamma * np.amax( self.model.predict( next_state)[0])

            y = self.model.predict(state)
            y[0][action] = target

            y_mini_batch.append(y)
            x_mini_batch.append(state)

        x_mini_batch = np.array(x_mini_batch).reshape([self.batch_size, self.n_inputs])
        y_mini_batch = np.array(y_mini_batch).reshape([self.batch_size, self.n_outputs])

        loss = self.model.fit(x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean(loss.history['loss'])

    def save_model(self, filepath):
        self.model.save(filepath)

    def load_model(self, filepath):
        self.model = load_model(filepath)

def convert_betting_observation( obs):
    bet_value = obs['Card history'][-1]
    return bet_value/5

def game_decision_step(game, player):
    game.deal_cards()

    state, reward, done = game.observe_game()
    if 'Current slot' not in state:
        print( state)

    while True:
        action = player.action(state)
        game.game_step(action)

        next_state, reward, done = game.observe_game()

        if done:
            break
        state = next_state

    return reward


def runner(game, game_decision, betting_decision, n_slots=1, n_epochs=150, n_max_games=1000, traning=False):
    data = {}
    for epoch in range(n_epochs):
        game.reset_iteration()
        print('epoch: {}, start money: {}'.format(epoch, game.money))
        money = []

        actions = []

        obs = game.reset()
        state = convert_betting_observation( obs)
        for game_index in range(n_max_games):
            action = betting_decision.action( state)
            actions.append( action)
            bet = betting_decision.calculate_bet(action, game.money)
            bet = [ bet] * n_slots

            game.set_bets( bet)
            rewards = game_decision_step(game, game_decision)

            money.append( game.money)

            obs = game.reset()
            next_state = convert_betting_observation( obs)
            if traning:
                betting_decision.remember(
                    state,
                    action,
                    np.sum( rewards),
                    next_state,
                    (game.money < 1) or (game_index + 1 == n_max_games)
                )

                loss = betting_decision.train()

                if game_index % 250 == 0:
                    print('game index: {}, epsilon: {}, loss: {}'.format(game_index, betting_decision.epsilon, loss))

            if game.money < 1:
                break

            state = next_state

        print('1: {}, 5: {}, 20: {}, 50: {}, 70: {}'.format(
            actions.count(0),
            actions.count(1),
            actions.count(2),
            actions.count(3),
            actions.count(4)
        ))
        print('end index: {}, end game money: {}'.format(game_index, game.money))

        if len( money) != n_max_games:
            missing_size = n_max_games - len( money)
            money.extend( [0] * missing_size)
        print()
        data['Game ' + str( epoch)] = money

    return pd.DataFrame( data), betting_decision

n_slots = 1
n_decks = 6
n_epochs = 50

n_games = 1000
money = 1000

game_decision = bjp.BasicPlayer()
betting_decision = BettingDecisionNN(1, 5, epsilon_start=0)
# betting_decision.load_model('betting-decision.h5')

plot_model(betting_decision.model, to_file='model.png', show_shapes=True)
print( betting_decision.model.summary())

'''
game = bj.BlackJack()
game.make(n_decks, start_money=money, num_slots=n_slots)

results, betting_decision = runner(game, game_decision, betting_decision, n_epochs=n_epochs, n_max_games=n_games)
# betting_decision.save_model('betting-decision.2.h5')
results.to_csv('testing_data.csv')
'''