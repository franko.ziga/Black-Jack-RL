from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU, PReLU
from keras.optimizers import Adam

from sklearn.preprocessing import OneHotEncoder

import matplotlib.pyplot as plt

import random
import numpy as np
from collections import deque

import Game.BJCards as bjc

class GameDecision:
    def __init__(self, learning_rate=0.001, lr_decay=0.000832, memory_length=8000, batch_size=64,
                 gamma=0.99, start_epsilon=1., min_epsilon=0.1, epsilon_decay=0.9998,
                 clipping_value=1.7, positive_reward_addition=1., dropout_rate=0.1):
        self.n_inputs = 118
        self.n_actions = 5

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, lr_decay, clipping_value)
        self.memory = self._create_replay_memory( memory_length)

    def _build_netowrk(self, learning_rate, lr_decay, clipping_value):
        model = Sequential()
        model.add( Dense( 256, activation='linear', input_dim=self.n_inputs))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 64, activation='linear'))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( self.n_actions, activation='linear'))
        model.compile(loss='mae', optimizer=Adam(lr=learning_rate))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 5
        elif hand.shape[0] == 2:
            return 4
        return 2

    def _epsilon_decay_step(self):
        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        possibilities = [1, 1, 1, 1, 3][:max_action]

        if np.random.rand() < self.epsilon:
            return np.random.choice([0, 1, 2, 4, 3][:max_action], p=possibilities/np.sum(possibilities))

        model_action = self.model.predict( x)[0]
        return np.argmax( model_action)

    def train(self):
        if self.batch_size > len( self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done in mini_batch:
            if done:
                target = reward
            else:
                target = reward + self.gamma * np.amax( self.model.predict( next_state)[0])
            y = self.model.predict( state)
            y[0][action] = target

            y_mini_batch.append( y)
            x_mini_batch.append( state)

        x_mini_batch = np.array( x_mini_batch).reshape( [self.batch_size, self.n_inputs])
        y_mini_batch = np.array( y_mini_batch).reshape( [self.batch_size, self.n_actions])

        loss = self.model.fit( x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean( loss.history[ 'loss'])

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done):
        self.memory.append( (state, action, reward, next_state, done))

    def repare_rewards(self, end_rewards):
        state, action, reward, next_state, done = self.memory[-1]

        if end_rewards[0] > 0:
            end_rewards[0] += self.positive_reward_addition
        elif end_rewards[0] < 0:
            end_rewards[0] -= self.positive_reward_addition

        self.memory[-1] = (state, action, end_rewards[0], next_state, done)


def convert_inputs( state, card_encoder, n_card_slots=8):
    x = card_encoder.transform( [state[ 'Dealer']]).toarray() # 13 inputs

    n_current_slot = len( state['Current slot'])
    hand = [[card] for card in state[ 'Current slot']]

    encoded_hand = card_encoder.transform( hand).toarray()
    if n_current_slot < n_card_slots:
        zeros = np.zeros((n_card_slots - n_current_slot, 13))
        encoded_hand = np.concatenate([encoded_hand, zeros])

    else:
        encoded_hand = encoded_hand[:n_card_slots, :]

    x = np.concatenate([x, encoded_hand]).flatten() # 13 * 8 = 104
    x = np.append(x, state['Deck value'])
    return np.reshape(x, [1, len(x)]) # 104 + 13 + 1 = 118

def create_one_hot_encoder():
    card_encoder = OneHotEncoder()
    values = [[val] for val in range(1, 14, 1)]
    card_encoder.fit( values)
    return card_encoder

state = {}
state['Dealer'] = [5]
state['Current slot'] = np.array([6, 6])
state['Deck value'] = 10/17

card_encoder = create_one_hot_encoder()
model = GameDecision(start_epsilon=0)
model.load_model( 'game-decision.33.h5')

input = convert_inputs( state, card_encoder)
predictions = model.action(input, state['Current slot'])
# print( 'State: ', state)
# print( 'Predictions: ', predictions)

card_interpretation = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
'''
for card1 in range(1, 14):
    for card2 in range(1, 14):
        predictions = []
        for dealer_card in range(1, 14):
            deck = bjc.Deck(0)
            deck.addCard( bjc.Card('H', card_interpretation[card1-1]))
            deck.addCard( bjc.Card('H', card_interpretation[card2-1]))
            state['Dealer'] = [dealer_card]
            state['Current slot'] = deck.encodedDeck()
            state['Deck value'] = deck.getDeckValue()/17

            input = convert_inputs(state, card_encoder)
            predictions.append( model.action( input, state['Current slot']))

        print('{}, {} -> {}'.format(
            card_interpretation[ card1-1],
            card_interpretation[ card2-1],
            predictions
        ))
'''

pl_deck = bjc.Deck(0)
pl_deck.addCard( bjc.Card('H', card_interpretation[ 4]))
pl_deck.addCard( bjc.Card('H', card_interpretation[ 4]))

state = {}
state['Dealer'] = [4]
state['Current slot'] = pl_deck.encodedDeck()
state['Deck value'] = pl_deck.getDeckValue()

input = convert_inputs(state, card_encoder)
out = model.model.predict( input)
print('Predictions: ', out)