import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf
import pandas as pd

import Game.BJGame as bjg
import Models.RGCollector as rgc
import Models.BJGameBotNetwork as models

import matplotlib.pyplot as plt
import pickle


def display(collector, epoch, display=True):
    if not display: return
    epoch_data = collector.epoch_mean(epoch)
    print('Epoch: ', epoch_data[ 'Epoch'])
    print('End bet: {}, mean game money: {}, mean played bets: {}'.
          format( bets[0], epoch_data[ 'Current money'], epoch_data[ 'Game index']))
    print('Mean bet: {}, standard deviation of bets: {}'.
          format( epoch_data[ 'Game bet'], epoch_data['Bet std']))
    print('mean game loss: {}'.format( epoch_data['Game loss']))
    #print('winning predictions: {}, winning prediction loss: {}'.
    #      format(epoch_data['Winning predictor'], epoch_data['Winning loss']))

def plot_graphs(collector):
    x, attribute_mean = collector.model_analysis()

    #plt.figure()
    #average = pd.rolling_mean( attribute_mean[ 'Game reward'], window=int(n_epoches / 10), min_periods=0)
    #plt.title( 'Mean reward')
    #plt.ylabel('Epoch mean reward')
    #plt.xlabel('Iteration')
    #plt.plot(x, attribute_mean[ 'Game reward'], label='iteration mean reward')
    #plt.plot( average, label='rolling mean of mean reward')
    #plt.legend(loc='best')


    plt.figure()
    average = pd.rolling_mean( attribute_mean['Current money'], window=int(n_epoches / 10), min_periods=0)
    plt.title( 'End money')
    plt.ylabel('End of game money')
    plt.xlabel('Iteration')
    plt.plot( attribute_mean[ 'Current money'])
    plt.plot( average, label='rolling mean of money: ' + str( int( n_epoches/10)))
    plt.legend(loc='best')

    #plt.figure()
    #plt.title( 'Cost overtime')
    #plt.ylabel('Game Cost')
    #plt.xlabel('Iteration')
    #plt.plot( attribute_mean['Game loss'], label='game loss')
    # plt.plot( attribute_mean['Betting loss'], label='betting loss')
    #plt.legend(loc='best')
    plt.show()

# game spacifications
debug = False

model_folder = 'Saved models/current trainerv2 model/'
model_file = model_folder + 'full model'

n_decks = 8
n_epoches = 10
n_games_per_epoch = 10
n_max_bets = 100
n_card_slots = 8
start_money = n_max_bets * 3

max_history = 5
n_slots = 3
bet_normalization_factor = n_max_bets

game_spacification = {}
game_spacification[ 'n_decks'] = n_decks
game_spacification[ 'n_epoches'] = n_epoches
game_spacification[ 'n_games_per_epoch'] = n_games_per_epoch
game_spacification[ 'n_max_bets'] = n_max_bets
game_spacification[ 'n_card_slots'] = n_card_slots
game_spacification[ 'start_money'] = start_money
game_spacification[ 'max_history'] = max_history
game_spacification[ 'n_slots'] = n_slots
game_spacification[ 'bet_normalization_factor'] = bet_normalization_factor


# hipperparameter for game bot
n_x, n_y = 11, 4
learning_rate = 0.05
bot_discount_rate = 0.95
layer_dimensions = [ 80, 50, n_y]
activation_functions = [ tf.nn.relu, tf.nn.relu, tf.nn.softmax]
game_decision = models.BJGameBotNetwork(n_x, n_y,
                                        layer_dimensions,
                                        activation_functions,
                                        initializer=tf.contrib.layers.xavier_initializer(),
                                        learning_rate=learning_rate)

game_bot = {}
game_bot[ 'n_x'] = n_x
game_bot[ 'n_y'] = n_y
game_bot[ 'learning_rate'] = learning_rate
game_bot[ 'layer_dimensions'] = layer_dimensions
game_bot[ 'activation_functions'] = activation_functions

# hipperparameters for betting bot
# 13 different cards, 1 current money
n_x, n_y = 22, 1 # max_history * n_slots + 13 + 6, 1
learning_rate = 0.0001
bet_discount_rate = 0.9 #(max_history - 1) / max_history # 0.8
layer_dimensions = [ 300, n_y]
activation_functions = [tf.nn.relu] * len( layer_dimensions)
distribution = 'exp'
mean_start = 0.
std_random = 4.
decay = 0.1
exp_random = 1.
bet_maker = models.BJBettingBot(n_x,
                                layer_dimensions,
                                activation_functions,
                                distribution='exp',
                                mean_start=0.,
                                std_random=4.,
                                dropout_rate=0.1,
                                exp_random=1.,
                                normalizator=bet_normalization_factor,
                                initializer=tf.contrib.layers.xavier_initializer(),
                                learning_rate=learning_rate)

betting_bot = {}
betting_bot[ 'n_x'] = n_x
betting_bot[ 'n_y'] = n_y
betting_bot[ 'learning_rate'] = learning_rate
betting_bot[ 'layer_dimensions'] = layer_dimensions
betting_bot[ 'activation_functions'] = activation_functions
betting_bot[ 'distribution'] = distribution
betting_bot[ 'mean_start'] = mean_start
betting_bot[ 'std_random'] = std_random
betting_bot[ 'decay'] = decay
betting_bot[ 'exp_random'] = exp_random

with open( model_folder + 'Game.specification.pkl', 'wb') as file:
    pickle.dump(game_spacification, file)

with open( model_folder + 'Game.bot.specification.pkl', 'wb') as file:
    pickle.dump(game_bot, file)

with open( model_folder + 'Betting.bot.specification.pkl', 'wb') as file:
    pickle.dump(betting_bot, file)

# game initialization
game = bjg.BlackJack()
game.make(n_decks, normalizator=bet_normalization_factor, start_money=start_money, render=False, max_history=max_history)

init = tf.global_variables_initializer()
model_collector = rgc.ModelAnalysis(['Epoch', 'Game bet', 'Game index', 'Current money', 'Game reward', 'Game loss', 'Play index'])#, 'Winning predictor', 'Winning loss'])
saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run( init)

    for epoch in range( n_epoches):
        collector = rgc.GBCollector(game_discount_rate=bot_discount_rate, bet_discount_rate=bet_discount_rate)

        for play_games in range( n_games_per_epoch):
            # resetting player, dealer and deck
            game.reset_iteration()

            for game_index in range( n_max_bets):

                if game.money <= 0:
                    print( 'End game by game money < 0, money: ', game.money)
                    break
                # resetting all cards and bets
                obs = game.reset()

                current_bets = [0] * n_slots
                x = bet_maker.convert_observationsv2( obs, game_index, 0, current_bets, max_game=n_max_bets, max_history=max_history)

                #winning_chance, winning_loss, winning_gradient = sess.run( [bet_maker.winning_traning_action,
                #                                                            bet_maker.winning_loss,
                #                                                            bet_maker.winning_gradients],
                #                                                            feed_dict={bet_maker.X: x})

                betting_gradients = []
                for bet_index in range( n_slots):
                    # print( x)
                    bet, bet_gradients = sess.run( [bet_maker.train_y_pred,
                                                    bet_maker.gradients], feed_dict={bet_maker.X: x})
                    betting_gradients.append( bet_gradients)
                    current_bets[ bet_index] = bet[0][0]
                    x = bet_maker.convert_observationsv2( obs, game_index, bet_index+1, current_bets, max_game=n_max_bets, max_history=max_history)

                # adding gradients to collector, rewards are computed after!
                collector.bet_step( betting_gradients)

                # converting bets and setting them to the game
                bets = np.array( current_bets) * bet_normalization_factor
                if debug: print( 'Bets: ', bets)
                done = game.set_bets( bets)

                if done:
                    print( 'End game by zeros bet')
                    break

                # dealing pre game cards
                game.deal_cards()
                obs, reward, done = game.observe_game()
                slot_index = obs[ 'Slot index']
                if debug: print( 'observation: ', obs)
                # safety check
                if slot_index == -1:
                    print( 'Slot index -1')
                    break

                game_loss = []
                collector.new_slot()

                while True:
                    # converting observationg in real input
                    x = game_decision.convert_observations(obs, n_card_slots=n_card_slots)
                    # training action with gradient values and current cost
                    action, gradients_val, current_cost = sess.run([game_decision.traning_action,
                                                                    game_decision.gradients,
                                                                    game_decision.cost], feed_dict={game_decision.X: x})
                    game_loss.append( current_cost)
                    action = action[0][0]
                    # preforming step in game
                    viable = game.game_step(action)
                    if debug: print( 'action: ', action)
                    # new observations
                    obs, reward, done = game.observe_game()
                    # saving values
                    if debug: print( 'reward: {}, done: {}'.format( reward, done))

                    collector.game_step(0, gradients_val)
                    collector.game_action(action, viable)

                    if obs[ 'Slot index'] != slot_index:
                        collector.new_slot()
                        slot_index = obs[ 'Slot index']

                    if done or obs['Slot index'] == -1:
                        break

                # applying end rewards to main collector
                # print( 'bets: ', bets)
                collector.end_rewards( reward, bets)

                model_collector.insert_row({'Epoch': epoch,
                                            'Current money': game.money,
                                            'Game index': game_index,
                                            'Game reward': np.sum( reward),
                                            'Game loss': np.mean( game_loss),
                                            'Play index': play_games,
                                            'Game bet': bets})
        display(model_collector, epoch, display=True)
        # normalizing rewards
        print( 'Normalization rewards')
        collector.discount_rewards()

        print( 'Calculating mean gradient')
        # calculating mean gradient
        bet_feed_dict = bet_maker.calculate_mean_gradientv2( collector.bet_gradients,
                                                             collector.normalized_bet_rewards)
        bot_feed_dict = game_decision.calculate_mean_gradient(collector.game_gradients,
                                                              collector.normalized_game_rewards)
        print( 'Feeding data to trainer')
        # running trainer to apply gradients
        sess.run(game_decision.trainer, feed_dict=bot_feed_dict)
        sess.run(bet_maker.trainer, feed_dict=bet_feed_dict)
        print()
    path = saver.save(sess, model_file + '.ckpt')

plot_graphs( model_collector)