import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf
import pandas as pd

import Game.BJGame as bjg
import NeuralNetwork.RGCollector as rgc
import NeuralNetwork.BJGameBotNetwork as models

import matplotlib.pyplot as plt


def display(collector, epoch, display=True):
    if not display: return
    epoch_data = collector.epoch_mean(epoch)
    print('End bets: ', bets[0], ', game money: ', epoch_data['Current money'], ' game index: ', epoch_data['Game index'])
    print('Epoch: {}, mean bet: {}, mean game loss: {}, mean bet loss: {}'.format(
        epoch_data['Epoch'],
        epoch_data[ 'Game bet'],
        epoch_data['Game loss'],
        epoch_data['Betting loss'])
    )

def plot_graphs(collector):
    x, attribute_mean = collector.model_analysis()

    plt.figure()
    average = pd.rolling_mean( attribute_mean[ 'Game reward'], window=int(n_epoches / 10), min_periods=0)
    plt.title( 'Mean reward')
    plt.ylabel('Epoch mean reward')
    plt.xlabel('Iteration')
    plt.plot(x, attribute_mean[ 'Game reward'], label='iteration mean reward')
    plt.plot( average, label='rolling mean of mean reward')
    plt.legend(loc='best')


    plt.figure()
    average = pd.rolling_mean( attribute_mean['Current money'], window=int(n_epoches / 10), min_periods=0)
    plt.title( 'End money')
    plt.ylabel('End of game money')
    plt.xlabel('Iteration')
    plt.plot( attribute_mean[ 'Current money'])
    plt.plot( average, label='rolling mean of money: ' + str( int( n_epoches/10)))
    plt.legend(loc='best')

    plt.figure()
    plt.title( 'Cost overtime')
    plt.ylabel('Game Cost')
    plt.xlabel('Iteration')
    plt.plot( attribute_mean['Game loss'], label='game loss')
    plt.plot( attribute_mean['Betting loss'], label='betting loss')
    plt.legend(loc='best')
    plt.show()

# game spacifications
n_decks = 8
n_epoches = 30
n_games_per_epoch = 20
n_max_bets = 300
n_card_slots = 8
start_money = 300

max_history = 20
n_slots = 3
bet_normalization_factor = start_money


# hipperparameter for game bot
n_x, n_y = 11, 4
learning_rate = 0.05
bot_discount_rate = 0.95
layer_dimensions = [ 80, 50, n_y]
activation_functions = [ tf.nn.relu, tf.nn.relu, tf.nn.softmax]
game_decision = models.BJGameBotNetwork(n_x, n_y,
                                        layer_dimensions,
                                        activation_functions,
                                        initializer=tf.contrib.layers.xavier_initializer(),
                                        learning_rate=learning_rate)

# hipperparameters for betting bot
# 13 different cards, 1 current money
'''
n_x, n_y = max_history * n_slots + 1 + 2, n_slots
learning_rate = 0.01
bet_discount_rate = 0.8 #(max_history - 1) / max_history
layer_dimensions = [ 500, 500, 400, n_y]
activation_functions = [tf.nn.relu] * len( layer_dimensions)
bet_maker = models.BJBettingBot(n_x,
                                layer_dimensions,
                                activation_functions,
                                mean_start=0.,
                                std_random=3.,
                                decay=0.2,
                                exp_random=2,
                                normalizator=bet_normalization_factor,
                                initializer=tf.contrib.layers.xavier_initializer(),
                                learning_rate=learning_rate)
'''

n_steps = max_history
n_inputs = 18
n_outputs = n_slots

learning_rate = 0.01
bet_discount_rate = (max_history - 1) / max_history
layer_dimensions = [200, n_outputs]
bet_maker = models.BJRecurrentBettingBot(n_steps,
                                         n_inputs,
                                         layer_dimensions,
                                         learning_rate=learning_rate,
                                         std_random=3.,
                                         decay=.1,
                                         normalizator=bet_normalization_factor)

# epoch_collector = rgc.EpochData()

# game initialization
game = bjg.BlackJack()
game.make(n_decks, normalizator=bet_normalization_factor, start_money=start_money, render=False)

init = tf.global_variables_initializer()
model_collector = rgc.ModelAnalysis(['Epoch', 'Game bet', 'Game index', 'Current money', 'Game reward', 'Betting loss', 'Game loss', 'Play index'])
with tf.Session() as sess:
    sess.run( init)
    mean_bets = []

    for epoch in range( n_epoches):
        game_rgcollector = rgc.BotRGCollector( bet_discount_rate)
        bet_rgcollector = rgc.BetRGCollector( bot_discount_rate)
        # epoch data containing array of tuples game_bets, #games, sum(rewards), end_money, bet_loss, game_loss
        bts, epoch_games = [], []

        for play_games in range( n_games_per_epoch):
            # resetting player, dealer and deck
            game.reset_iteration()

            for game_index in range( n_max_bets):
                if game.money <= 0:
                    print( 'End game by game money < 0, money: ', game.money)
                    break
                # resetting all cards and bets
                obs = game.reset()
                x = bet_maker.convert_observations( obs, game_index, max_game=n_max_bets, max_history=max_history*2)
                bets, output, bet_gradients, bet_loss = sess.run( [bet_maker.train_y_pred,
                                                                                   bet_maker.output,
                                                                                   bet_maker.gradients,
                                                                                   bet_maker.loss], feed_dict={bet_maker.X: x})
                # epoch_collector.epoch_bet_loss.append( bet_loss)
                bets = bets * bet_normalization_factor
                bts.append( bets[0])
                # print( bets)
                done = game.set_bets( bets[0])

                if done:
                    print( 'End game by zeros bet')
                    break

                game.deal_cards()
                obs, reward, done = game.observe_game()
                slot_index = obs[ 'Slot index']

                if slot_index == -1:
                    print( 'Slot index -1')
                    break

                game_loss = []
                game_rgcollector.new_slot()

                while True:
                    # converting observationg in real input
                    x = game_decision.convert_observations(obs, n_card_slots=n_card_slots)
                    # training action with gradient values and current cost
                    action, gradients_val, current_cost = sess.run([game_decision.traning_action,
                                                                    game_decision.gradients,
                                                                    game_decision.cost], feed_dict={game_decision.X: x})
                    game_loss.append( current_cost)
                    # preforming step in game
                    viable = game.game_step(action)
                    # new observations
                    obs, reward, done = game.observe_game()
                    # saving values
                    game_rgcollector.slot_append(reward, gradients_val)
                    # epoch_collector.game_cost_append( current_cost)

                    if obs[ 'Slot index'] != slot_index:
                        game_rgcollector.new_slot()
                        slot_index = obs[ 'Slot index']

                    if done or obs['Slot index'] == -1:
                        break

                # appending rewards to rgcollector and bet_rg_collector
                game_rgcollector.apply_end_rewards( reward)
                bet_rgcollector.appendRG( reward, bet_gradients)

                model_collector.insert_row({'Epoch': epoch,
                                            'Current money': game.money,
                                            'Betting loss': bet_loss,
                                            'Game index': game_index,
                                            'Game reward': np.sum( reward),
                                            'Game loss': np.mean( game_loss),
                                            'Game bet': bets[0],
                                            'Play index': play_games})
            # bet_rgcollector.rewards[-1][-1] = game.money - game.starting_money
        display(model_collector, epoch, display=True)
        # normalizing rewards
        print( 'Normalization rewards')
        game_rgcollector.discount_normalize_rewards()
        bet_rgcollector.compute_rewards()

        print( 'Calculating mean gradient')
        # calculating mean gradient
        bet_feed_dict = bet_maker.calculate_mean_gradient( bet_rgcollector.gradients,
                                                           bet_rgcollector.gradient_rewards)
        bot_feed_dict = game_decision.calculate_mean_gradient(game_rgcollector.gradients,
                                                              game_rgcollector.rewards)
        print( 'Feeding data to trainer')
        # running trainer to apply gradients
        sess.run(game_decision.trainer, feed_dict=bot_feed_dict)
        sess.run(bet_maker.trainer, feed_dict=bet_feed_dict)
        print()

plot_graphs( model_collector)