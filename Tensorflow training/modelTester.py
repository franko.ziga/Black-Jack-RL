import numpy as np
import tensorflow as tf
import pandas as pd

import Game.BJGame as bjg
import Models.RGCollector as rgc
import Models.BJGameBotNetwork as bjgb

import matplotlib.pyplot as plt
import pickle


def plot_graphs(collector):
    attribute_mean = collector.tester_analysis(start_money)

    #plt.figure()
    #average = pd.rolling_mean( attribute_mean[ 'Game reward'], window=int(n_epoches / 10), min_periods=0)
    #plt.title( 'Mean reward')
    #plt.ylabel('Epoch mean reward')
    #plt.xlabel('Iteration')
    #plt.plot(x, attribute_mean[ 'Game reward'], label='iteration mean reward')
    #plt.plot( average, label='rolling mean of mean reward')
    #plt.legend(loc='best')


    plt.figure()
    plt.title( 'End money')
    plt.ylabel('Current money')
    plt.xlabel('Game index')
    plt.plot( attribute_mean[ 'Current money'], label='End game money')
    # plt.plot( attribute_mean[ 'Game index'], label='End game index')
    average = pd.rolling_mean( attribute_mean[ 'Current money'], window=10, min_periods=0)
    plt.plot( average, label='Average')
    plt.legend(loc='best')

    #plt.figure()
    #plt.title ( 'Bets over time')
    #plt.ylabel( 'bet')
    #plt.xlabel( 'Game index')

    #print( attribute_mean.keys())
    #print( attribute_mean[ 'Game bet'])

    #bets = np.transpose( attribute_mean[ 'Game bet'])
    #for index, bet in enumerate( bets):
    #    plt.plot( bet, label='bet index: ' + str( index))
    #plt.legend(loc='best')

    #plt.figure()
    #plt.title( 'Cost overtime')
    #plt.ylabel('Game Cost')
    #plt.xlabel('Iteration')
    #plt.plot( attribute_mean['Game loss'], label='game loss')
    # plt.plot( attribute_mean['Betting loss'], label='betting loss')
    #plt.legend(loc='best')
    plt.show()

def game_convert_observations(obs, n_card_slots=8):
    '''
    :param obs:
    :return: one row array which contains dealer card (0), slot cards (1-9) and card history (9-21)
    '''
    max_card_value = 11
    if 'Current bet' not in obs:
        print( obs)

    slot_bet     = [obs[ 'Current bet']]
    dealer_card  = obs[ 'Dealer']
    slot_cards   = obs[ 'Current slot']
    card_history = obs[ 'Card history'][-1]

    n_current_slot = len(slot_cards)

    if n_current_slot <= n_card_slots:
        difference = [0] * (n_card_slots - n_current_slot)
        slot_cards = np.concatenate([slot_cards, difference])
    else:
        slot_cards = slot_cards[:n_card_slots]

    x = np.append( dealer_card/max_card_value, slot_cards/max_card_value)
    x = np.append( x, card_history)
    x = np.append( x, np.array( slot_bet))
    # x = np.concatenate([dealer_card / max_card_value, slot_cards / max_card_value, card_history, slot_bet])
    return x.reshape((1, len(x)))

def bet_convert_observations(observations, game_index, bet_index, current_bets, max_game=1000, max_history=20):
    bet_history = np.array(observations['Bet history'])
    card_history = observations['Card history'][-1]
    money = np.array(observations['Current money'][-1])
    end_game_activator = np.array([0 if game_index < max_game - max_history else (game_index - max_game + max_history) / max_history])

    x = np.append( money, card_history)
    x = np.append( x, bet_history)
    x = np.append( x, end_game_activator)
    x = x.reshape((1, len(x)))

    x = np.append(x[0], np.array( [bet_index]), axis=0)
    x = np.append(x, np.array( current_bets), axis=0)
    return x.reshape((1, len( x)))

def repair_bets( bets):
    if np.sum( bets) == 0:
        bets[ 0] = 1. / bet_normalization_factor

    return bets

def load_model_spacifications( folder):
    with open( folder + 'Game.bot.specification.pkl', 'rb') as file:
        game_bot_spacification = pickle.load( file)

    with open( folder + 'Betting.bot.specification.pkl', 'rb') as file:
        betting_bot_specification = pickle.load( file)

    with open( folder + 'Game.specification.pkl', 'rb') as file:
        game_spacification = pickle.load( file)

    return game_spacification, game_bot_spacification, betting_bot_specification

debug = False

folder = 'Saved models/current trainerv2 model/'
game_spec, game_bot_spec, betting_bot_spec = load_model_spacifications( folder)

n_decks = game_spec[ 'n_decks']
n_games_per_epoch = 100
n_max_bets = 100
n_card_slots = game_spec[ 'n_card_slots']
start_money = n_max_bets * 3

max_history = game_spec[ 'max_history']
n_slots = game_spec[ 'n_slots']
bet_normalization_factor = game_spec[ 'bet_normalization_factor']

game_bot = bjgb.BJGameBotNetwork(
    game_bot_spec[ 'n_x'],
    game_bot_spec[ 'n_y'],
    game_bot_spec[ 'layer_dimensions'],
    game_bot_spec[ 'activation_functions']
)

betting_bot = bjgb.BJBettingBot(
    betting_bot_spec[ 'n_x'],
    betting_bot_spec[ 'layer_dimensions'],
    betting_bot_spec[ 'activation_functions'],
    distribution=betting_bot_spec[ 'distribution'],
    mean_start=betting_bot_spec[ 'mean_start'],
    std_random=betting_bot_spec[ 'std_random'],
    dropout_rate=betting_bot_spec[ 'decay'],
    exp_random=betting_bot_spec[ 'exp_random'],
    normalizator=bet_normalization_factor,
    learning_rate=betting_bot_spec[ 'learning_rate']
)


game = bjg.BlackJack()
game.make(n_decks, normalizator=bet_normalization_factor, start_money=start_money, render=False, max_history=max_history)

model_collector = rgc.ModelAnalysis(['Game bet', 'Game index', 'Current money', 'Game reward', 'Play index'], end_tester=True)

saver = tf.train.Saver()

with tf.Session() as sess:
    saver.restore(sess, folder + 'full model.ckpt')

    for play_games in range( n_games_per_epoch):
        # resetting player, dealer and deck
        game.reset_iteration()

        game_index = 0
        current_bets = [0.] * n_slots
        while True:
            if (game_index >= n_max_bets) and (np.sum( current_bets) <= 1):
                break
        # for game_index in range( n_max_bets):

            if game.money <= 0:
                print( 'End game by game money < 0, money: ', game.money)
                break
            # resetting all cards and bets
            obs = game.reset()

            current_bets = [0] * n_slots
            x = bet_convert_observations(obs, game_index, 0, current_bets, max_game=n_max_bets, max_history=max_history)

            betting_gradients = []
            for bet_index in range( n_slots):
                bet = sess.run( betting_bot.predict, feed_dict={betting_bot.X: x})
                current_bets[ bet_index] = bet[0][0]
                x = bet_convert_observations(obs, game_index, bet_index+1, current_bets, max_game=n_max_bets, max_history=max_history)

            # converting bets and setting them to the game
            current_bets = repair_bets( current_bets)

            bets = np.array( current_bets) * bet_normalization_factor
            if debug: print( 'Bets: ', bets)

            # Tried to add square to current bets but its not helping
            # bets = np.array( [round( bet ** 1.2) for bet in bets])
            done = game.set_bets( bets)

            if done:
                print( 'End game by zeros bet')
                break

            # dealing pre game cards
            game.deal_cards()
            obs, reward, done = game.observe_game()
            slot_index = obs[ 'Slot index']
            if debug: print( 'observation: ', obs)
            # safety check
            if slot_index == -1:
                print( 'Slot index -1')
                break

            while True:
                # converting observationg in real input
                x = game_convert_observations(obs, n_card_slots=n_card_slots)
                # training action with gradient values and current cost
                action = sess.run(game_bot.action, feed_dict={game_bot.X: x})
                action = action[0]
                # preforming step in game
                viable = game.game_step(action)
                if debug: print( 'action: ', action)
                # new observations
                obs, reward, done = game.observe_game()
                # saving values
                if debug: print( 'reward: {}, done: {}'.format( reward, done))

                if obs[ 'Slot index'] != slot_index:
                    slot_index = obs[ 'Slot index']

                if done or obs['Slot index'] == -1:
                    break

            model_collector.insert_row({'Current money': game.money,
                                        'Game index': game_index,
                                        'Game reward': np.sum( reward),
                                        'Play index': play_games,
                                        'Game bet': bets})

            game_index += 1

plot_graphs( model_collector)