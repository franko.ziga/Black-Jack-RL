import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf

import Game.BJGame as bjg
import Models.RGCollector as rgc
import Models.BJGameBotNetwork as models

import os
import pickle

def save_specifications(game_spec, player_spec, betting_spec, folder):
    with open(folder + 'Game.specification.pkl', 'wb') as file:
        pickle.dump(game_spec, file)

    player_spec.pop('activation functions')
    with open(folder + 'Game.bot.specification.pkl', 'wb') as file:
        pickle.dump(player_spec, file)

    betting_spec.pop('activation functions')
    with open(folder + 'Betting.bot.specification.pkl', 'wb') as file:
        pickle.dump(betting_spec, file)

def create_models(game_spec, player_spec, betting_spec):
    game_model = models.BJGameBotNetwork(
        player_spec[ 'n x'],
        player_spec[ 'n y'],
        player_spec['layer dimensions'],
        player_spec['activation functions'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=player_spec[ 'learning rate']
    )

    betting_model = models.BJBettingBot(
        betting_spec[ 'n x'],
        betting_spec['layer dimensions'],
        betting_spec['activation functions'],
        distribution=betting_spec[ 'distribution'],
        mean_start=betting_spec[ 'mean'],
        std_random=betting_spec[ 'std'],
        dropout_rate=betting_spec[ 'dropout rate'],
        exp_random=betting_spec[ 'exp random'],
        normalizator=game_spec[ 'normalization'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=betting_spec[ 'learning rate']
    )

    return game_model, betting_model

def create_game(game_spec):
    game = bjg.BlackJack()
    game.make(
        game_spec['n decks'],
        normalizator=game_spec[ 'normalization'],
        max_history=game_spec[ 'max history'],
        start_money=game_spec[ 'start money']
    )
    return game

def create_random_layers(last_layer, min_units=50, max_units=500, min_layers=1, max_layers=5):
    n_layers = np.random.randint(min_layers, max_layers, 1)[0]
    layer_dimensions = []

    for index in range(n_layers):
        if index > 0:
            max_units = layer_dimensions[ -1]
            if max_units == min_units:
                break
        layer_dimensions.append( np.random.randint(min_units, max_units,1)[0])

    layer_dimensions.append( last_layer)
    return layer_dimensions

def create_model_parameters():
    game_spec = {}

    game_spec[ 'n slots'] = 3
    game_spec[ 'n decks'] = 8
    game_spec[ 'n epochs'] = np.random.randint(10, 30, 1)[0]
    game_spec[ 'n games per epoch'] = 10
    game_spec[ 'n max bets'] = 100
    game_spec[ 'start money'] = game_spec[ 'n max bets'] * game_spec[ 'n slots']
    game_spec[ 'max history'] = np.random.randint(5, 10, 1)[0]
    game_spec[ 'normalization'] = game_spec[ 'n max bets']

    player_spec = {}
    player_spec[ 'n card slots'] = 8
    player_spec[ 'n x'] = 11
    player_spec[ 'n y'] = 4
    player_spec[ 'discount rate'] = 1 - 10 ** (-np.random.uniform(low=1, high=2))
    player_spec[ 'layer dimensions'] = create_random_layers(player_spec['n y'], min_units=15, max_units=100, max_layers=3)
    player_spec[ 'activation functions'] = [tf.nn.relu] * (len( player_spec[ 'layer dimensions'])-1)
    player_spec[ 'activation functions'].append( tf.nn.softmax)
    player_spec[ 'learning rate'] = 10 ** (-np.random.uniform(low=2, high=3) * 5)

    betting_spec = {}
    betting_spec[ 'n x'] = 31 #game_spec[ 'max history'] * game_spec[ 'n slots'] + 13 + 6
    betting_spec[ 'layer dimensions'] = create_random_layers(1, min_units=20, max_layers=4)
    betting_spec[ 'activation functions'] = [tf.nn.relu] * len( betting_spec[ 'layer dimensions'])
    betting_spec[ 'discount rate'] = 1 - 10 ** (-np.random.uniform(low=2, high=3))
    betting_spec[ 'distribution'] = 'exp'
    betting_spec[ 'mean'] = np.float32(np.random.rand(1)[0] * 3)
    betting_spec[ 'std'] = np.float32(np.random.rand(1)[0] * 3)
    betting_spec[ 'dropout rate'] = np.random.uniform(low=0.2, high=0.4)
    betting_spec[ 'exp random'] = np.float32( np.random.uniform(low=1., high=3.))
    betting_spec[ 'learning rate'] = 10 ** (-np.random.uniform(low=2., high=3.))

    return game_spec, player_spec, betting_spec

def make_directory(folder_path):
    if not os.path.exists( folder_path):
        os.makedirs( folder_path, exist_ok=True)

def next_directory(folder_base):
    models = [int(model_name.split(' ')[1]) for model_name in os.listdir( folder_base)
              if os.path.isdir(folder_base + model_name)]
    if len( models) == 0:
        model_name = 'Model 0'
    else:
        last_number = int( sorted( models)[-1]) + 1
        model_name = 'Model ' + str( last_number)
    path = folder_base + model_name + '/'
    make_directory(path)
    return path

def prepare_folder(folder_path):
    make_directory( folder_path)
    new_folder_path = next_directory( folder_path)
    return new_folder_path

def black_jack_betting(sess, game, bet_maker, collector, game_spec, game_index=0):
    '''
    :param sess: we need session to run out betting model
    :param game: in which we are in currently
    :param bet_maker: predicting model
    :param collector: of model behavior
    :param game_spec: game specifications
    :return: updated collector, current bets, and if done
    '''
    # resetting all cards and bets
    obs = game.reset()

    current_bets = [0] * game_spec[ 'n slots']
    x = bet_maker.convert_observationsv2(obs, game_index, 0, current_bets, max_game=game_spec[ 'n max bets'], max_history=game_spec[ 'max history'])

    betting_gradients = []
    for bet_index in range(game_spec[ 'n slots']):
        bet, bet_gradients = sess.run([bet_maker.train_y_pred,
                                       bet_maker.gradients], feed_dict={bet_maker.X: x})
        betting_gradients.append(bet_gradients)
        current_bets[bet_index] = bet[0][0]
        x = bet_maker.convert_observationsv2(obs, game_index, bet_index + 1, current_bets, max_game=game_spec[ 'n max bets'],
                                             max_history=game_spec[ 'max history'])

    # adding gradients to collector, rewards are computed after!
    collector.bet_step(betting_gradients)

    # converting bets and setting them to the game
    bets = np.array(current_bets) * game_spec[ 'normalization']
    done = game.set_bets(bets)

    return collector, bets, done

def black_jack_card_decisions(sess, game, game_decision, collector, player_spec):
    # dealing pre game cards
    game.deal_cards()
    obs, reward, done = game.observe_game()
    slot_index = obs['Slot index']

    game_loss = []
    collector.new_slot()

    while True:
        # converting observationg in real input
        x = game_decision.convert_observations(obs, n_card_slots=player_spec['n card slots'])
        # training action with gradient values and current cost
        action, gradients_val, current_cost = sess.run([game_decision.traning_action,
                                                        game_decision.gradients,
                                                        game_decision.cost], feed_dict={game_decision.X: x})
        game_loss.append(current_cost)
        action = action[0][0]
        # preforming step in game
        viable = game.game_step(action)
        # new observations
        obs, reward, done = game.observe_game()

        collector.game_step(0, gradients_val)
        collector.game_action(action, viable)

        if obs['Slot index'] != slot_index:
            collector.new_slot()
            slot_index = obs['Slot index']

        if done or obs['Slot index'] == -1:
            break

    return collector, reward, game_loss

def black_jack_full_game(sess, game, collector, game_decision, bet_maker, game_spec, player_spec, game_index=0):
    collector, current_bets, done = black_jack_betting(sess, game, bet_maker, collector, game_spec, game_index=game_index)

    if done:
        return

    collector, reward, game_loss = black_jack_card_decisions(sess, game, game_decision, collector, player_spec)

    # applying end rewards to main collector
    collector.end_rewards(reward, current_bets)

    return collector, current_bets, reward

def train_models(sess, bet_maker, game_decision, collector):
    # normalizing rewards
    collector.discount_rewards()

    # calculating mean gradient
    bet_feed_dict = bet_maker.calculate_mean_gradientv2(collector.bet_gradients,
                                                        collector.normalized_bet_rewards)
    bot_feed_dict = game_decision.calculate_mean_gradient(collector.game_gradients,
                                                          collector.normalized_game_rewards)
    # running trainer to apply gradients
    sess.run(game_decision.trainer, feed_dict=bot_feed_dict)
    sess.run(bet_maker.trainer, feed_dict=bet_feed_dict)

def insert_game_results(collector, current_money, game_reward, current_bet, epoch, game_index, play_index):
    collector.insert_row({'Epoch': epoch,
                          'Current money': current_money,
                          'Game index': game_index,
                          'Game reward': np.sum(game_reward),
                          'Play index': play_index,
                          'Game bet': current_bet})
    return collector

def display_specification(game_spec, player_spec, betting_spec):
    print('Game specifications')
    for key in game_spec.keys():
        print('\t{}: {}'.format(key, game_spec[ key]))
    print()
    print('Player specifications')
    for key in player_spec.keys():
        print('\t{}: {}'.format(key, player_spec[key]))
    print()
    print('Betting specifications')
    for key in betting_spec.keys():
        print('\t{}: {}'.format(key, betting_spec[key]))
    print()


def train_new_model(base_folder='Trained models/', model_name='model'):
    tf.reset_default_graph()
    game_spec, player_spec, betting_spec = create_model_parameters()
    display_specification(game_spec, player_spec, betting_spec)

    game_decision, bet_maker = create_models(game_spec, player_spec, betting_spec)
    game = create_game(game_spec)

    model_collector = rgc.ModelAnalysis(
        ['Epoch', 'Game bet', 'Game index', 'Current money', 'Game reward', 'Play index'])

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run( init)

        for epoch in range( game_spec[ 'n epochs']):
            if epoch % 5 == 0:
                print('\tepoch: ', epoch)
            collector = rgc.GBCollector(game_discount_rate=player_spec['discount rate'],
                                        bet_discount_rate=betting_spec['discount rate'])

            for game_index in range( game_spec['n games per epoch']):
                game.reset_iteration()

                for play_index in range( game_spec['n max bets']):
                    if game.money <= 0:
                        break

                    collector, current_bet, current_rewards = black_jack_full_game(
                        sess, game, collector, game_decision, bet_maker, game_spec, player_spec, game_index=play_index
                    )
                    model_collector = insert_game_results(
                        model_collector, game.money, current_rewards, current_bet, epoch, game_index, play_index
                    )

            train_models(sess, bet_maker, game_decision, collector)

        new_folder = prepare_folder(base_folder)

        save_specifications(game_spec, player_spec, betting_spec, new_folder)
        saver.save(sess, new_folder + model_name + '.ckpt')
        model_collector.save_analysis( new_folder)

for index in range(30):
    print( 'training model with index: ', index)
    train_new_model()