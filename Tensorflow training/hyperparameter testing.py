import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf

import Game.BJGame as bjg
import Models.RGCollector as rgc
import Models.BJGameBotNetwork as models

import os
import pickle

MODEL_FILE = 'model.ckpt'
MODEL_TESTER_FILE = 'testing analysis.csv'
BASE_FOLDER = 'Trained models/'

# ------------ INITIALIZATION STEP ------------

# Full model load
# getting 3 dictionaris (game_spec, player_spec, betting_spec)
# which contains all information model needs to be build or rebuild
def load_model_specifications(folder):
    with open(folder + 'Game.specification.pkl', 'rb') as file:
        game_spec = pickle.load(file)

    with open(folder + 'Game.bot.specification.pkl', 'rb') as file:
        player_spec = pickle.load(file)
    player_spec['activation functions'] = [tf.nn.relu] * (len( player_spec['layer dimensions'])-1)
    player_spec['activation functions'].append( tf.nn.softmax)

    with open(folder + 'Betting.bot.specification.pkl', 'rb') as file:
        betting_spec = pickle.load( file)
    betting_spec['activation functions'] = [tf.nn.relu] * len( betting_spec[ 'layer dimensions'])

    return game_spec, player_spec, betting_spec

# creating game based on game specifications
def create_game(game_spec):
    game = bjg.BlackJack()
    game.make(
        game_spec['n decks'],
        normalizator=game_spec[ 'normalization'],
        max_history=game_spec[ 'max history'],
        start_money=game_spec[ 'start money']
    )
    return game

# convetring specifications into models
def create_models(game_spec, player_spec, betting_spec):
    game_model = models.BJGameBotNetwork(
        player_spec[ 'n x'],
        player_spec[ 'n y'],
        player_spec['layer dimensions'],
        player_spec['activation functions'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=player_spec[ 'learning rate']
    )

    betting_model = models.BJBettingBot(
        betting_spec[ 'n x'],
        betting_spec['layer dimensions'],
        betting_spec['activation functions'],
        distribution=betting_spec[ 'distribution'],
        mean_start=betting_spec[ 'mean'],
        std_random=betting_spec[ 'std'],
        dropout_rate=betting_spec[ 'dropout rate'],
        exp_random=betting_spec[ 'exp random'],
        normalizator=game_spec[ 'normalization'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=betting_spec[ 'learning rate']
    )

    return create_game(game_spec), game_model, betting_model

# ------------ MODEL PLAY BLACK JACK ------------

def black_jack_betting(sess, game, bet_maker, game_spec, game_index=0):
    '''
    placing bets before the game starts
    :return: current bets, and if done
    '''
    # resetting all cards and bets
    obs = game.reset()

    current_bets = [0] * game_spec[ 'n slots']
    x = bet_maker.convert_observationsv2(obs, game_index, 0, current_bets, max_game=game_spec[ 'n max bets'], max_history=game_spec[ 'max history'])

    for bet_index in range(game_spec[ 'n slots']):
        bet = sess.run(bet_maker.predict, feed_dict={bet_maker.X: x})
        current_bets[bet_index] = repair_bet(bet[0][0], game_spec[ 'normalization'])
        x = bet_maker.convert_observationsv2(obs, game_index, bet_index + 1, current_bets, max_game=game_spec[ 'n max bets'],
                                             max_history=game_spec[ 'max history'])

    # converting bets and setting them to the game
    bets = np.array(current_bets) * game_spec[ 'normalization']
    done = game.set_bets(bets)

    return bets, done

def repair_bet( bet, normalizator):
    if bet == 0:
        bet = 1. / normalizator
    return bet

def black_jack_card_decisions(sess, game, game_decision, player_spec):
    # dealing pre game cards
    game.deal_cards()
    obs, reward, done = game.observe_game()

    while True:
        # converting observationg in real input
        x = game_decision.convert_observations(obs, n_card_slots=player_spec['n card slots'])
        # true action of current step
        action = sess.run(game_decision.action, feed_dict={game_decision.X: x})
        action = action[0]

        # preforming step in game
        viable = game.game_step(action)

        # new observations
        obs, reward, done = game.observe_game()

        if done or obs['Slot index'] == -1:
            break

    return reward

def black_jack_full_game(sess, game, game_decision, bet_maker, game_spec, player_spec, game_index=0):
    current_bets, done = black_jack_betting(sess, game, bet_maker, game_spec, game_index=game_index)

    if done:
        return

    reward = black_jack_card_decisions(sess, game, game_decision, player_spec)

    return game.money, current_bets, reward


# ----------- MODEL TESTER ------------

def insert_game_results(collector, current_money, game_reward, current_bet, game_index, play_index):
    collector.insert_row({
        'Current bet': current_bet,
        'Current money': current_money,
        'Current reward': np.sum(game_reward),
        'Game index': game_index,
        'Play index': play_index,
    })
    return collector

def display_specification(game_spec, player_spec, betting_spec):
    print('Game specifications')
    for key in game_spec.keys():
        print('\t{}: {}'.format(key, game_spec[ key]))
    print()
    print('Player specifications')
    for key in player_spec.keys():
        print('\t{}: {}'.format(key, player_spec[key]))
    print()
    print('Betting specifications')
    for key in betting_spec.keys():
        print('\t{}: {}'.format(key, betting_spec[key]))
    print()

def testing_model(model_folder, game_length=100, n_games=100, reload=False, model_file='model.ckpt', base_folder='Trained models/'):
    path = base_folder + model_folder + '/'

    # if model was tested, don't test it again
    if os.path.exists( path + MODEL_TESTER_FILE) and (not reload):
        print( '{} was already tested!'.format( model_folder))
        return

    # resetting default graph so we can build a new one
    tf.reset_default_graph()

    # loading specifications on which model was trained
    game_spec, player_spec, betting_spec = load_model_specifications( path)
    display_specification(game_spec, player_spec, betting_spec)

    # creating back trained models
    game, game_decision, bet_maker = create_models(game_spec, player_spec, betting_spec)

    # model analysis will help us to compare models in future
    model_analysis = rgc.ModelAnalysis(['Current bet', 'Current money', 'Current reward', 'Game index', 'Play index'],
                                       end_tester=True)

    # restoring trained parameters
    savet = tf.train.Saver()
    with tf.Session() as sess:
        savet.restore(sess, path + model_file)

        # playing n_games * game_length of bets!
        for game_index in range( n_games):
            game.reset_iteration()

            for play_index in range( game_length):
                if game.money <= 0:
                    break

                current_bets, done = black_jack_betting(sess, game, bet_maker, game_spec, game_index=game_index)

                if done:
                    return

                current_rewards = black_jack_card_decisions(sess, game, game_decision, player_spec)

                model_analysis = insert_game_results(
                    model_analysis,
                    game.money,
                    current_rewards,
                    current_bets,
                    game_index,
                    play_index
                )

        print(path + MODEL_TESTER_FILE)
        model_analysis.save_analysis(path, file_name=MODEL_TESTER_FILE)


# going through all models and testing them
for model in os.listdir( BASE_FOLDER):
    if 'Model' in model:
        print( '{} is being tested!'.format( model))
        testing_model(
            model,
            game_length=100,
            n_games=1,
            base_folder=BASE_FOLDER,
            reload=True
        )

#testing_model('Model 66', game_length=100, n_games=50, base_folder=BASE_FOLDER, reload=True)
