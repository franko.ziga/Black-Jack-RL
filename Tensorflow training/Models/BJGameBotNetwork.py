import tensorflow as tf
import numpy as np

class BasicNetwork:

    def __init__(self, n_input, layer_dim, activation_fun, initializer, learning_rate):
        self.X = tf.placeholder(tf.float32, [None, n_input], name='X')
        self.learning_rate = learning_rate
        self.output, self.logits, self.hidden_layers = self.initialize_hidden_layers(self.X, layer_dim, activation_fun, initializer)

    def initialize_hidden_layers(self, X, layers_dim, act_functs, initializer):
        hidden_layers = []
        L = len(layers_dim)

        prev = X
        for index, n_layer, l_activation in zip(range(1, L + 1), layers_dim, act_functs):
            with tf.variable_scope( 'layer_' + str(index)):
                if isinstance(initializer, list):
                    z_layer = tf.layers.dense(prev, n_layer, kernel_initializer=initializer[ index-1])
                else:
                    z_layer = tf.layers.dense(prev, n_layer, kernel_initializer=initializer)
                # z_layer = tf.layers.batch_normalization( z_layer, training=True, momentum=0.9) # Batch normalization
                a_layer = l_activation(z_layer)
                # a_layer = tf.layers.dropout(a_layer, 0.5, training=True) # Dropout
            prev = a_layer

            hidden_layers.append(a_layer)

        output, hidden_layers = hidden_layers[-1], hidden_layers[:-1]
        logits = tf.identity(z_layer, name='logits')
        output = tf.identity(output, name='output')

        return output, logits, hidden_layers

    def initialize_gradient_placeholder(self, grad_vars):
        with tf.variable_scope( 'Bot_Gradients'):
            gradient_placeholders = []
            grad_vars_feed = []

            gradients = [grad for grad, var in grad_vars]
            for gradient, variable in grad_vars:
                gradient_placeholder = tf.placeholder(tf.float32, gradient.shape)

                gradient_placeholders.append(gradient_placeholder)
                grad_vars_feed.append((gradient_placeholder, variable))

            return gradients, gradient_placeholders, grad_vars_feed

class BJGameBotNetwork( BasicNetwork):

    def __init__(self, n_input, n_output, layer_dim, activation_fun, initializer=tf.contrib.layers.xavier_initializer(), learning_rate=0.01):
        with tf.variable_scope( 'Game_bot'):
            super().__init__(n_input, layer_dim, activation_fun, initializer, learning_rate)

            # treaing and real time action
            self.traning_action = tf.multinomial( tf.log(self.output), num_samples=1)
            self.action = tf.argmax( self.output, axis=1)
            tf.identity( self.action, name='action')

            y_pred = tf.one_hot( self.traning_action, n_output)
            # cost function with optimizer
            self.cost = tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, labels=y_pred)
            optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)

            # gradients
            grad_vars = optimizer.compute_gradients( self.cost)

            self.gradients, self.gradients_placeholder, gradient_pl_var_feed = self.initialize_gradient_placeholder(grad_vars)

            # traning step
            self.trainer = optimizer.apply_gradients(gradient_pl_var_feed)

    def calculate_mean_gradient(self, all_gradients, all_rewards):
        feed_dict = {}

        # all_rewards = np.array( all_rewards)
        # all_gradients = np.array( all_gradients)
        # print( all_gradients.shape)

        vectorized_reward = np.vectorize(lambda reward, gradient, var_index: gradient[:,:, var_index] * reward)

        for var_index, gradient_placeholder in enumerate( self.gradients_placeholder):
            #gradient_rewards = all_gradients[:,:, var_index] * all_rewards
            gradient_rewards = [reward * all_gradients[game_index][step][var_index]
                   for game_index, rewards in enumerate(all_rewards)
                   for step, reward in enumerate(rewards)]

            #var_arr = [var_index] * len( all_rewards)
            #gradient_rewards = map(lambda reward,gradient,var_index: print( gradient),
            #                       all_rewards, all_gradients, var_arr)
            #gradient_rewards = np.fromiter( 0, dtype=float)

            mean_gradients = np.mean(gradient_rewards, axis=0)
            feed_dict[gradient_placeholder] = mean_gradients

        return feed_dict

    def convert_observations(self, obs, n_card_slots=8):
        '''
        :param obs:
        :return: one row array which contains dealer card (0), slot cards (1-9) and card history (9-21)
        '''
        max_card_value = 11
        if 'Current bet' not in obs:
            print( obs)

        slot_bet     = [obs[ 'Current bet']]
        dealer_card  = obs[ 'Dealer']
        slot_cards   = obs[ 'Current slot']
        card_history = obs[ 'Card history'][-1]

        n_current_slot = len(slot_cards)

        if n_current_slot <= n_card_slots:
            difference = [0] * (n_card_slots - n_current_slot)
            slot_cards = np.concatenate([slot_cards, difference])
        else:
            slot_cards = slot_cards[:n_card_slots]

        x = np.append( dealer_card/max_card_value, slot_cards/max_card_value)
        x = np.append( x, card_history)
        x = np.append( x, np.array( slot_bet))
        # x = np.concatenate([dealer_card / max_card_value, slot_cards / max_card_value, card_history, slot_bet])
        return x.reshape((1, len(x)))


class BJBettingBot(BasicNetwork):
    def __init__(self, n_input, layer_dim, activation_fun, initializer=tf.contrib.layers.xavier_initializer(), distribution='normal', mean_start=1, std_random=2, exp_random=3, normalizator=1,
             learning_rate=0.01, dropout_rate=0.1, winning_learning_rate=0.01):
        with tf.variable_scope('Betting_bot'):
            super().__init__(n_input, layer_dim, activation_fun, initializer, learning_rate)

            # creating decays for random distributions and learning rate
            # self.create_wining_percentage_predictor(winning_learning_rate, initializer)
            self.create_decays(dropout_rate=dropout_rate, std_start=std_random)
            self.create_outputs(layer_dim[-1], distribution, mean_start, exp_random, normalizator)

            # cost function with optimizer
            self.loss_function()
            self.create_trainer()

    def create_wining_percentage_predictor(self, learning_rate, initializer):
        with tf.variable_scope('Winning_bot'):
            self.winning_placeholder = tf.placeholder(tf.float32, [None, 1], name='Y_wining_placeholder')

            self.winning_learning_rate = learning_rate

            self.winning_logits = tf.layers.dense(self.hidden_layers[-1], 1, kernel_initializer=initializer)
            self.winning_output = tf.sigmoid( self.winning_logits)

            self.winning_traning_action = tf.multinomial( tf.log( self.winning_output), num_samples=1)

            self.winning_loss = tf.nn.sigmoid_cross_entropy_with_logits(logits=self.winning_logits, labels=self.winning_placeholder)

            self.traning_action = tf.multinomial(tf.log(self.output), num_samples=1)
            self.action = tf.argmax(self.output)

            # cost function with optimizer
            optimizer = tf.train.AdamOptimizer(learning_rate=self.winning_learning_rate)

            # gradients
            grad_vars = optimizer.compute_gradients( self.winning_loss)
            grad_vars = [(weight, bais) for weight, bais in grad_vars if weight != None]
            self.winning_gradients, self.winning_gradients_placeholder, gradient_pl_var_feed = self.initialize_gradient_placeholder(grad_vars)

            # traning step
            self.winning_trainer = optimizer.apply_gradients(gradient_pl_var_feed)

    def loss_function(self):
        self.loss = tf.losses.mean_squared_error( self.train_y_pred, self.output)

    def create_trainer(self):
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)

        # gradients
        grad_vars = optimizer.compute_gradients(self.loss)

        grad_vars = [(weight, bais) for weight, bais in grad_vars if weight != None]
        self.gradients, self.gradients_placeholder, gradient_pl_var_feed = self.initialize_gradient_placeholder(
            grad_vars)

        # traning step
        self.trainer = optimizer.apply_gradients(gradient_pl_var_feed, global_step=self.global_step)

    def create_outputs(self, n_output, distribution, mean_start, exp_random, normalizator):
        # bet action functions
        self.predict = tf.round(self.output * normalizator) / normalizator
        tf.identity( self.predict, name='output')
        # print( self.predict)

        random_values = self.random_values(n_output, distribution=distribution, mean_start=mean_start,
                                           exp_random=exp_random)
        self.train_y_pred = tf.round(self.output * normalizator + random_values) / normalizator
        # feeding train_y_pred trough relu so we don't have any negative bet
        self.train_y_pred = tf.nn.relu(self.train_y_pred)

        ''''''
        # repering if sum is equal 0
        minimum_bet = [0] * n_output
        minimum_bet[-1] = 1
        minimum_bet = np.array( minimum_bet) / normalizator
        zero = tf.constant(0, dtype=tf.float32, name='zero.bet.checker')

        self.train_y_pred = tf.cond(tf.equal(tf.reduce_sum(self.train_y_pred), zero),
                                    lambda: self.train_y_pred + minimum_bet, # True
                                    lambda: self.train_y_pred,               # False
                                    name='zero.bet.condition')


    def create_decays(self, dropout_rate=0.2, std_start=2.):
        self.global_step = tf.Variable(0, trainable=False, name='Global_step')
        # Still not know if it would be smart to implement learning rate decay TODO!
        # self.learning_rate = tf.train.inverse_time_decay( learning_rate_start, self.global_step, learning_rate_start)
        # self.learning_rate = tf.train.exponential_decay(learning_rate_start, self.global_step, num_itterations, decay_start, staircase=True)

        self.std_epoch = std_start# tf.train.inverse_time_decay(std_start, self.global_step, 1, decay_rate)
        self.random_dropout = dropout_rate# tf.train.inverse_time_decay(0.33, self.global_step, 1, decay_rate)
        # self.std_epoch = std_start / (1 + decay_rate * self.global_step)

    def random_values(self, size, distribution='normal', mean_start=0, exp_random=3.):
        random_dropout = tf.random_uniform( [size])
        random_dropout = tf.cast( tf.less_equal( random_dropout, self.random_dropout), dtype=tf.float32)
        if distribution == 'normal':
            random_values = tf.distributions.Normal(loc=mean_start, scale=self.std_epoch).sample([size]) * random_dropout
        elif distribution == 'exp':
            random_values = tf.distributions.Exponential(rate=1.).sample( [size]) * exp_random * random_dropout
        else:
            random_values = [0] * size
        return random_values

    def convert_observations(self, observations, game_index, max_game=1000, max_history=20):
        bet_history = np.array( observations[ 'Bet history'])
        card_history = observations[ 'Card history'][-1]
        money = np.array( observations['Current money'][-1])
        end_game_activator = np.array( [0 if game_index < max_game-max_history else (game_index - max_game + max_history)/max_history])

        x = np.append( money, card_history)
        x = np.append( x, bet_history)
        x = np.append( x, end_game_activator)
        return x.reshape((1, len(x)))

    def convert_observationsv2(self, observations, game_index, bet_index, current_bets, max_game=1000, max_history=20):
        x = self.convert_observations(observations, game_index, max_game=max_game, max_history=max_history)
        x = np.append(x[0], np.array( [bet_index]), axis=0)
        x = np.append(x, np.array( current_bets), axis=0)
        return x.reshape((1, len( x)))

    def calculate_mean_gradient(self, all_gradients, all_rewards):
        feed_dict = {}
        all_gradients = np.array( all_gradients)
        all_rewards = np.array( all_rewards)

        for var_index, gradient_placeholder in enumerate( self.gradients_placeholder):
            gradient_rewards = all_gradients[:, var_index] * all_rewards
            mean_gradients = np.mean(gradient_rewards, axis=0)
            feed_dict[gradient_placeholder] = mean_gradients

        return feed_dict

    def calculate_mean_gradientv2(self, all_gradients, all_rewards):
        feed_dict = {}

        all_gradients = np.array( all_gradients)
        all_rewards = np.array( all_rewards)

        for var_index, gradient_placeholder in enumerate( self.gradients_placeholder):
            gradient_rewards = [reward * all_gradients[game_index][step][var_index]
                                for game_index, rewards in enumerate(all_rewards)
                                for step, reward in enumerate(rewards)]

            mean_gradients = np.mean(gradient_rewards, axis=0)
            feed_dict[gradient_placeholder] = mean_gradients

        return feed_dict

class BJRecurrentBettingBot(BJBettingBot):
    def __init__(self, n_steps, n_input, layer_dim, distribution='normal', mean_start=0., std_random=2., normalizator=1., learning_rate=0.01, decay=0.1):
        with tf.variable_scope('BJ_LSTM_Betting_bot'):
            self.X = tf.placeholder(tf.float32, [None, n_steps, n_input], name='X')
            self.learning_rate = learning_rate

            self.output, hidden_layers = self.initialize_LSTM_layers(layer_dim, RNN_n_layers=3, RNN_n_neurons=150)

            # creating decays for random distributions and learning rate
            self.create_decays(dropout_rate=decay, std_start=std_random)
            self.create_outputs(layer_dim[-1], distribution, mean_start, 0, normalizator)

            # cost function with optimizer
            self.loss_function()
            self.create_trainer()

    def initialize_LSTM_layers(self, layer_dim, RNN_n_neurons=100, RNN_n_layers=3):
        layers = [tf.contrib.rnn.LSTMCell(RNN_n_neurons) for _ in range(RNN_n_layers)]
        multiple_layer_cell = tf.contrib.rnn.MultiRNNCell( layers)

        X = tf.unstack( tf.transpose( self.X, perm=[1,0,2]))
        output, states = tf.nn.static_rnn(multiple_layer_cell, X, dtype=tf.float32)
        output = tf.transpose(output, [1, 0, 2])

        prev = output[-1]
        hidden_layers = []
        for n_neurons in layer_dim:
            fc_layer = tf.layers.dense(prev, n_neurons, activation=tf.nn.relu, kernel_initializer=tf.contrib.layers.xavier_initializer())
            hidden_layers.append( fc_layer)
            prev = fc_layer

        output, hidden_layers = hidden_layers[-1], hidden_layers[:-1]
        return output, hidden_layers


    def convert_observations(self, observations, game_index, max_game=1000, max_history=20):
        bet_history = np.array( observations[ 'Bet history'])
        card_history = observations[ 'Card history']
        L = len( bet_history)

        money = np.array( [observations['Current money']]).reshape((L, 1))

        end_game_activator = np.array( [game_index-index for index in reversed( range( L))])
        end_game_activator[ end_game_activator < (max_game-max_history)] = 0
        end_game_activator[ end_game_activator > 0] =  end_game_activator[ end_game_activator > 0] - max_game + max_history
        end_game_activator = np.array( [end_game_activator/max_history]).reshape((L,1))

        x = np.concatenate( (bet_history, card_history), axis=1)
        x = np.concatenate( (x, money), axis=1)
        x = np.concatenate( (x, end_game_activator), axis=1)

        return np.array( [x])