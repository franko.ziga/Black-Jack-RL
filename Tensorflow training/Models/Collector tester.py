import RGCollector as rgc


print( 'Testing insertion')
collector = rgc.GBCollector()
game_rewards = [[0, 0, 0], [0, 0], [0, 0, 0], [0]]

for sub_reward in game_rewards:
    collector.new_slot()
    for reward in sub_reward:
        collector.game_step( reward, 0)

assert (game_rewards == collector.game_reward)
print()
print( 'Testing end game rewards 1:')
splits = [0, 0, 1, 0]

bet_rewards = [5, -1, 4, 2]
bets = [5, 1, 2]

true_result = [[0, 0, 5], [0, -1], [2, 0, 4], [2]]
print( '\tgame rewards: {},\n\tsplits: {},\n\tend rewards: {},\n\ttrue result: {}'.format(game_rewards, splits, bet_rewards, true_result))

collector.game_reward = game_rewards
collector.split_actions = splits

collector.end_rewards( bet_rewards, bets)
print('\toutput: {}'.format( collector.game_reward))
assert (true_result == collector.game_reward)

print()
print( 'Testing end bet rewards:')
true_bet_result = [[5, -1, 6]]
print( '\tTrue bet results: {}\n\toutput: {}'.format(true_bet_result, collector.bet_reward))

assert (true_bet_result == collector.bet_reward)


print()
print( 'Testing end game rewards 2:')
collector = rgc.GBCollector()
game_rewards = [[0, 0, 0], [0, 0]]
collector.game_reward = game_rewards

splits = [1, 0]

bet_rewards = [2, 1]
bets = [0, 1, 0]

true_result = [[1, 0, 2], [0, 1]]
print( '\tgame rewards: {},\n\tsplits: {},\n\tend rewards: {},\n\ttrue result: {}'.format(game_rewards, splits, bet_rewards, true_result))

collector.game_reward = game_rewards
collector.split_actions = splits

collector.end_rewards( bet_rewards, bets)
print('\toutput: {}'.format( collector.game_reward))
assert (true_result == collector.game_reward)

print()
print( 'Testing end bet rewards:')
true_bet_result = [[0,3,0]]
print( '\tTrue bet results: {}\n\toutput: {}'.format(true_bet_result, collector.bet_reward))

assert (true_bet_result == collector.bet_reward)





print( )
print('Testing discount rewards:')
collector.discount_rewards()

print( 'Normalized game rewards: ', collector.normalized_game_rewards)
print( 'Normalized bet rewards: ', collector.normalized_bet_rewards)


print()
print( 'All tests passed!')
