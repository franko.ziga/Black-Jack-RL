'''
Reward & Gradient Collector, data for graphs of each epoch
'''
import numpy as np
import pandas as pd
from itertools import accumulate


class GBCollector:

    def __init__(self, game_discount_rate=0.95, bet_discount_rate=0.95):
        self.game_discount_rate, self.bet_discount_rate = game_discount_rate, bet_discount_rate
        self.reset()

    def reset(self):
        self.game_gradients, self.bet_gradients = [], []
        self.game_reward, self.bet_reward = [], []
        self.winning_gradients = []
        self.reset_game()

    def reset_game(self):
        self.split_actions = []

    def winning_predictor(self, gradient):
        self.winning_gradients.append( gradient)

    def new_slot(self):
        self.game_gradients.append( [])
        self.game_reward.append( [])
        self.split_actions.append( 0)

    def game_action(self, action, valid):
        '''
        checking if action was split, if so save current index
        :param action: model action
        '''
        if (action == 3) and valid:
            self.split_actions[-1] = 1

    def game_step(self, current_reward, current_gradient):
        self.game_reward[-1].append( current_reward)
        self.game_gradients[-1].append( current_gradient)

    def bet_step(self, current_gradients):
        self.bet_gradients.append( current_gradients)

    def cut_down(self):
        self.game_gradients = self.game_gradients[:-1]
        self.game_reward    = self.game_reward[:-1]
        self.split_actions  = self.split_actions[:-1]

    def end_rewards(self, end_reward, bets):
        self.cut_down()
        n_bets = len( bets)
        n_slots = len( end_reward)
        n_rewards = len( self.game_reward)

        for index in range( n_slots):
            self.game_reward[ n_rewards - n_slots + index][-1] = end_reward[ index]

            if self.split_actions[ index] == 1:
                self.game_reward[ n_rewards - n_slots + index][0] += end_reward[ index+1]

        current_bet_reward = [0.] * n_bets

        steper = 0

        for index in range( n_bets):
            if bets[ index] == 0: continue
            current_bet_reward[ index] += end_reward[ steper]
            if self.split_actions[ steper] == 1:
                steper += 1
                current_bet_reward[ index] += end_reward[ steper]
            steper += 1

        self.bet_reward.append( current_bet_reward)
        self.reset_game()

    def apply_discount(self, rewards, discount_rate):
        acc = list(accumulate(rewards[::-1], lambda x, y: x * discount_rate + y))
        return np.array( acc[::-1])

    def apply_normalization(self, history):
        flat = np.concatenate( history)
        std = np.std( flat)
        # mean = np.mean( flat)

        return history / std

    def discount_rewards(self):
        self.normalized_game_rewards = [self.apply_discount(reward, self.game_discount_rate) for reward in self.game_reward]

        self.normalized_bet_rewards = np.transpose( self.bet_reward)
        self.normalized_bet_rewards = [self.apply_discount( reward, self.bet_discount_rate) for reward in self.normalized_bet_rewards]
        self.normalized_bet_rewards = np.transpose( self.normalized_bet_rewards)

        compressed_game_reward = np.sum( self.bet_reward, axis=1)
        self.normalized_winning_rewards = compressed_game_reward / np.std( compressed_game_reward)
        compressed_game_reward = self.apply_discount(compressed_game_reward, self.bet_discount_rate)
        compressed_game_reward = np.transpose( [compressed_game_reward / np.std( compressed_game_reward)] * len( self.normalized_bet_rewards[0]))
        # assert (self.normalized_bet_rewards.shape[0] == compressed_game_reward.shape[0])

        self.normalized_game_rewards = self.apply_normalization( self.normalized_game_rewards)
        self.normalized_bet_rewards = self.apply_normalization( self.normalized_bet_rewards) + compressed_game_reward

class BotRGCollector:

    def __init__(self, discount_rate):
        self.discount_rate = discount_rate
        self.rewards, self.gradients = [], []
        self.game_rewards, self.game_gradients = [], []
        self.splits, self.game_slots_actions = [], []

    def action(self, action, viable):
        '''
        checking if action was split, if so save current index
        :param action: model action
        '''
        if action == 3 and viable:
            self.game_slots_actions.append( 1)
        else:
            self.game_slots_actions.append( 0)

    def slot_append(self, reward, gradients):
        self.game_rewards[-1].append( reward)
        self.game_gradients[-1].append( gradients)

    def new_slot(self):
        self.game_rewards.append( [])
        self.game_gradients.append( [])

        self.splits.append( np.sum( self.game_slots_actions))
        self.game_slots_actions = []


    def apply_end_rewards(self, rewards):
        for index in reversed( range( len( rewards))):
            self.game_rewards[index][-1] = rewards[ index]

        self.rewards.extend( self.game_rewards)
        self.gradients.extend( self.game_gradients)

        self.game_rewards, self.game_gradients = [], []

    def discount_reward(self, rewards, discount_rate):
        acc = list(accumulate(rewards[::-1], lambda x, y: x * discount_rate + y))
        return np.array( acc[::-1])

    def repare_split_rewards(self, current_rewards, extra_rewards):
        '''reparing rewards if there was some splits in game'''
        start_rewards = [reward[ 0] for reward in extra_rewards]
        for index, reward in enumerate( reversed( start_rewards)):
            current_rewards[index] += reward
        return current_rewards

    def discount_normalize_rewards(self):
        all_discounted_rewards = [self.discount_reward(reward, self.discount_rate) for reward in self.rewards]
        all_discounted_rewards = [self.repare_split_rewards( all_discounted_rewards[ index], all_discounted_rewards[ index+1:index+split+1])
                                  if split > 0 else all_discounted_rewards[ index]
                                  for index, split in enumerate( self.splits)]
        flat = np.concatenate(all_discounted_rewards)
        mean = flat.mean()
        std = flat.std()

        self.rewards = (all_discounted_rewards - mean) / std

class BetRGCollector:

    def __init__(self, disount_rate):
        self.rewards = []
        self.gradients = []
        self.discount_rate = disount_rate

    def appendRG(self, reward, gradient):
        self.rewards.append( reward)
        self.gradients.append( gradient)

    def discount_reward(self, rewards, discount_rate):
        acc = list(accumulate(rewards[::-1], lambda x, y: x * discount_rate + y))
        return np.array( acc[::-1])

    def discount_normalize_rewards(self):
        all_discounted_rewards = [self.discount_reward(reward, self.discount_rate) for reward in np.transpose( self.rewards)]
        flat = np.concatenate( all_discounted_rewards)
        mean = flat.mean()
        std = flat.std()

        self.rewards = np.transpose( (all_discounted_rewards - mean) / std)

    def compute_rewards(self):
        self.discount_normalize_rewards()

        self.gradient_rewards = [np.sum( game_reward) for game_reward in self.rewards]
        self.gradient_rewards = self.discount_reward( self.gradient_rewards, self.discount_rate)

        self.rewards = []

class ModelAnalysis:
    def __init__(self, attributes, end_tester=False):
        self.last_columns = ['Current money']
        if not end_tester:
            self.last_columns.append( 'Game index')
        self.collector = pd.DataFrame({attribute: [] for attribute in attributes})

    def insert_row(self, row_dict):
        self.collector = self.collector.append( row_dict, ignore_index=True)

    def model_analysis(self, epoch_column='Epoch'):
        '''
        :param epoch_column:
        :param bet_column:
        :return: (indexes, dictionary containing each attribute with mean values at each epoch)
        '''
        group = self.collector.copy()

        max_mean = group.groupby( [epoch_column, 'Play index'])[ self.last_columns].last()
        max_mean.reset_index(inplace=True)
        max_mean = max_mean.groupby( epoch_column)[ self.last_columns].mean()

        group = group.groupby( epoch_column).mean()

        ret = {attribute: group[ attribute] for attribute in group.columns}
        ret.update({attribute: max_mean[ attribute] for attribute in max_mean.columns})
        return group.index, ret

    def epoch_mean(self, epoch):
        group = self.collector.copy()
        group = group[ group[ 'Epoch'] == epoch]

        std = group [ 'Game bet'].apply(lambda bet: np.array( bet))
        std = std.as_matrix().std()
        max_mean = group.groupby( 'Play index')[ self.last_columns].last()

        ret = {attribute: group[ attribute].mean() for attribute in group.columns}
        ret.update({attribute: max_mean[ attribute].mean() for attribute in max_mean.columns})
        ret[ 'Bet std'] = std
        return ret

    def tester_analysis(self, starting_money):
        group = self.collector.copy()

        # group[ 'Current money'] = group[ 'Current money'] - starting_money
        ret = {}
        ret[ 'Current money'] = group.groupby( 'Play index')['Current money'].last()
        ret[ 'Game index'] = group.groupby( 'Play index')['Game index'].last()
        #ret[ 'Game bet'] = group.groupby( 'Game index')['Game bet'].mean()

        return ret

    def save_analysis(self, folder, file_name='model analysis.csv'):
        self.collector.to_csv( folder+file_name, sep=';')
        print('data was saved as: {}'.format(folder+file_name))

    def load_model_analysis(self, folder, file_name='model analysis.csv'):
        self.collector = pd.read_csv(folder+file_name, sep=';')