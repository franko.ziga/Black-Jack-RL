import pandas as pd
import numpy as np
from ast import literal_eval
import pickle
import os

import matplotlib.pyplot as plt


def load_model_results( path, model_folder, analysis_file='testing analysis.csv'):
    file = path + model_folder + '/' + analysis_file
    model_data = pd.read_csv( file, sep=';')
    #a = model_data[ 'Current bet'].apply(lambda x: literal_eval(x.replace(' ', ', '))).as_matrix()
    #print( a)

    column_name = model_folder + ' money'
    model_data.rename(columns={'Current money': column_name}, inplace=True)
    model_data = model_data.groupby( 'Play index')[ column_name].mean()
    model_data = model_data.reset_index()
    model_data.drop('Play index', axis=1, inplace=True)

    return model_data

def load_full_data( base_folder='Trained models/'):

    data_collector = pd.DataFrame()
    index = 0
    for model_name in os.listdir( base_folder):
        if 'Model' in model_name:
            model_data = load_model_results(base_folder, model_name)
            column_name = model_data.columns[0]

            if data_collector.empty:
                data_collector = model_data
            else:
                data_collector[ column_name] = model_data[ column_name]

            index += 1

    return data_collector

def load_model_specifications(folder):
    with open(folder + 'Game.specification.pkl', 'rb') as file:
        game_spec = pickle.load(file)

    with open(folder + 'Game.bot.specification.pkl', 'rb') as file:
        player_spec = pickle.load(file)

    with open(folder + 'Betting.bot.specification.pkl', 'rb') as file:
        betting_spec = pickle.load( file)

    return game_spec, player_spec, betting_spec

def parameter_data( models, base_folder='Trained models/'):
    param_data = pd.DataFrame()

    for model in models:
        model_folder = base_folder + model + '/'
        game_spec, player_spec, betting_spec = load_model_specifications( model_folder)

        model_data = {
            'index': [model],
            'max history': [game_spec[ 'max history']],

            'player learning rate': [player_spec['learning rate']],
            'player discount rate': [player_spec['discount rate']],
            'player layers': [player_spec[ 'layer dimensions']],

            'betting layers': [betting_spec['layer dimensions']],
            'betting discount rate': [betting_spec[ 'discount rate']],
            'betting learning rate': [betting_spec[ 'learning rate']],
            'betting distribuion': [betting_spec[ 'distribution']],
            'mean': [betting_spec[ 'mean']],
            'std': [betting_spec[ 'std']],
            'exp random': [betting_spec[ 'exp random']],
            'dropout rate': [betting_spec[ 'dropout rate']]
        }

        model_data = pd.DataFrame( data=model_data)

        if param_data.empty:
            param_data = pd.DataFrame( model_data)
        else:
            param_data = param_data.append( model_data)

    param_data.set_index('index', inplace=True)
    return param_data.transpose()

data = load_full_data()
models = data.columns

# plt.legend(loc='best')


print( 'Top 10 models:')
# getting all models and their end money
scores = {model: data[ model].as_matrix()[-1] for model in data.keys()}
# sorting scores by success
final_scores = [(k, scores[k]) for k in sorted(scores, key=scores.get, reverse=True)]
# plotting top 10 models
print( '\n'.join([model + ': ' + str(score) for model, score in final_scores[:10]]))


print()
print( 'Parameter data of models')
models = [model[:-6] for model, _ in final_scores[:10]]
# print(models)
param_data = parameter_data(models)
print( param_data)



plt.figure()
plt.xlabel( 'Play index')
plt.ylabel( 'Money')

# plt.plot([300.] * 100)
for model in models:
    results = data[ model + ' money'].as_matrix()
    plt.plot(results, label=model)

plt.show()