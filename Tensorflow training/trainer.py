import sys
sys.path.append('../')

import numpy as np
import tensorflow as tf

import Game.BJCards as bjc
import Game.BJGame as bjg
import Models.RGCollector as rgc
import Models.BJGameBotNetwork as models

import os
import pickle

# -------- SAVING SPECIFICATIONS MODULES ------------

def save_specifications(game_spec, player_spec, betting_spec, folder):
    with open(folder + 'Game.specification.pkl', 'wb') as file:
        pickle.dump(game_spec, file)

    player_spec.pop('activation functions')
    with open(folder + 'Game.bot.specification.pkl', 'wb') as file:
        pickle.dump(player_spec, file)

    betting_spec.pop('activation functions')
    with open(folder + 'Betting.bot.specification.pkl', 'wb') as file:
        pickle.dump(betting_spec, file)

def make_directory(folder_path):
    if not os.path.exists( folder_path):
        os.makedirs( folder_path, exist_ok=True)


# ---------- DEBUG DISPLAY MODULES ---------
def display_betting_prestep(obs):
    print('Current money: {}'.format( obs['Current money']))
    print('Reward history: \n{}'.format( obs['Bet history']))
    print('Card distribution: ')
    for card_name, card_poss in zip(bjc.Card.viableValues(), obs[ 'Card history'][-1]):
        print('\t{} :: {}'.format(card_name, card_poss))

def display_betting_step(betting_index, current_bets):
    print('Bet for slot: {}, current bets: {}'.format(betting_index, current_bets))

def display_pregame(obs):
    print('Dealers hand: {}'.format( obs[ 'Dealer']))

def display_game_step(obs, action, viable):
    print('Slot: {}, cards: {} \n\t-> action: {}, ({})'.format(obs[ 'Slot index'], obs[ 'Current slot'], action, viable))

def display_end_game(money, rewards):
    print('Money after game: {}, game rewards: {}'.format(money, rewards))
    print()

def display_specification(game_spec, player_spec, betting_spec):
    print('Game specifications')
    for key in game_spec.keys():
        print('\t{}: {}'.format(key, game_spec[ key]))
    print()
    print('Player specifications')
    for key in player_spec.keys():
        print('\t{}: {}'.format(key, player_spec[key]))
    print()
    print('Betting specifications')
    for key in betting_spec.keys():
        print('\t{}: {}'.format(key, betting_spec[key]))
    print()

def input_stop():
    input('Press any to continue!')

# ---------- SELECTING MODEL PARAMETERS & BUILDING MODEL --------------
def create_model_parameters():
    game_spec = {}

    game_spec[ 'n slots'] = 3
    game_spec[ 'n decks'] = 8
    game_spec[ 'n epochs'] = 50
    game_spec[ 'n games per epoch'] = 1
    game_spec[ 'n max bets'] = 100
    game_spec[ 'start money'] = game_spec[ 'n max bets'] * game_spec[ 'n slots']
    game_spec[ 'max history'] = 8
    game_spec[ 'normalization'] = game_spec[ 'n max bets']

    player_spec = {}
    player_spec[ 'n card slots'] = 8
    player_spec[ 'n x'] = 11
    player_spec[ 'n y'] = 4
    player_spec[ 'discount rate'] = 0.99
    player_spec[ 'layer dimensions'] = [150, player_spec[ 'n y']]
    player_spec[ 'activation functions'] = [tf.nn.relu] * (len( player_spec[ 'layer dimensions'])-1)
    player_spec[ 'activation functions'].append( tf.nn.softmax)
    player_spec[ 'learning rate'] = 0.005

    betting_spec = {}
    betting_spec[ 'n x'] = 31 #game_spec[ 'max history'] * game_spec[ 'n slots'] + 13 + 6
    betting_spec[ 'layer dimensions'] = [2, 1]
    betting_spec[ 'activation functions'] = [tf.nn.relu] * len( betting_spec[ 'layer dimensions'])
    betting_spec[ 'discount rate'] = 0.95
    betting_spec[ 'distribution'] = 'exp'
    betting_spec[ 'mean'] = np.float32(np.random.rand(1)[0] * 3)
    betting_spec[ 'std'] = np.float32(np.random.rand(1)[0] * 3)
    betting_spec[ 'dropout rate'] = 0.33
    betting_spec[ 'exp random'] = 1.5
    betting_spec[ 'learning rate'] = 0.01

    return game_spec, player_spec, betting_spec

def create_models(game_spec, player_spec, betting_spec):
    game_model = models.BJGameBotNetwork(
        player_spec[ 'n x'],
        player_spec[ 'n y'],
        player_spec['layer dimensions'],
        player_spec['activation functions'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=player_spec[ 'learning rate']
    )

    betting_model = models.BJBettingBot(
        betting_spec[ 'n x'],
        betting_spec['layer dimensions'],
        betting_spec['activation functions'],
        distribution=betting_spec[ 'distribution'],
        mean_start=betting_spec[ 'mean'],
        std_random=betting_spec[ 'std'],
        dropout_rate=betting_spec[ 'dropout rate'],
        exp_random=betting_spec[ 'exp random'],
        normalizator=game_spec[ 'normalization'],
        initializer=tf.contrib.layers.xavier_initializer(),
        learning_rate=betting_spec[ 'learning rate']
    )

    return game_model, betting_model

def create_game(game_spec):
    game = bjg.BlackJack()
    game.make(
        game_spec['n decks'],
        normalizator=game_spec[ 'normalization'],
        max_history=game_spec[ 'max history'],
        start_money=game_spec[ 'start money']
    )
    return game


# ------------ BLACK-JACK SUB STEPS ------------
def black_jack_betting(sess, game, bet_maker, collector, game_spec, game_index=0, debug=False):
    # resetting all cards and bets
    obs = game.reset()

    if debug: display_betting_prestep(obs)

    current_bets = [0] * game_spec[ 'n slots']
    x = bet_maker.convert_observationsv2(obs, game_index, 0, current_bets, max_game=game_spec[ 'n max bets'], max_history=game_spec[ 'max history'])

    betting_gradients = []
    for bet_index in range(game_spec[ 'n slots']):
        if debug: display_betting_step(bet_index, current_bets)
        bet, bet_gradients = sess.run([bet_maker.train_y_pred,
                                       bet_maker.gradients], feed_dict={bet_maker.X: x})
        betting_gradients.append(bet_gradients)
        current_bets[bet_index] = bet[0][0]
        x = bet_maker.convert_observationsv2(obs, game_index, bet_index + 1, current_bets, max_game=game_spec[ 'n max bets'],
                                             max_history=game_spec[ 'max history'])

    # adding gradients to collector, rewards are computed after!
    collector.bet_step(betting_gradients)

    # converting bets and setting them to the game
    bets = np.array(current_bets) * game_spec[ 'normalization']
    bets = [1., 1., 1.]
    done = game.set_bets(bets)

    return collector, bets, done

def black_jack_card_decisions(sess, game, game_decision, collector, player_spec, debug=False):
    # dealing pre game cards
    game.deal_cards()
    obs, reward, done = game.observe_game()
    slot_index = obs['Slot index']

    if debug: display_pregame(obs)

    game_loss = []
    collector.new_slot()

    while True:
        # converting observationg in real input
        x = game_decision.convert_observations(obs, n_card_slots=player_spec['n card slots'])
        # training action with gradient values and current cost
        action, gradients_val, current_cost = sess.run([game_decision.traning_action,
                                                        game_decision.gradients,
                                                        game_decision.cost], feed_dict={game_decision.X: x})
        game_loss.append(current_cost)
        action = action[0][0]
        # preforming step in game
        viable = game.game_step(action)
        if debug: display_game_step(obs, action, viable)
        # new observations
        obs, reward, done = game.observe_game()

        collector.game_step(0, gradients_val)
        collector.game_action(action, viable)

        if obs['Slot index'] != slot_index:
            collector.new_slot()
            slot_index = obs['Slot index']

        if done or obs['Slot index'] == -1:
            break

    return collector, reward, game_loss

def black_jack_full_game(sess, game, collector, game_decision, bet_maker, game_spec, player_spec, game_index=0, debug=False):
    collector.reset_game()
    collector, current_bets, done = black_jack_betting(
        sess,
        game,
        bet_maker,
        collector,
        game_spec,
        game_index=game_index,
        debug=debug
    )

    if done:
        return

    collector, reward, game_loss = black_jack_card_decisions(
        sess,
        game,
        game_decision,
        collector,
        player_spec,
        debug=debug
    )

    #if len( reward) > 3:
    #    print( 'Game rewards: ', collector.game_reward)
    #    print( 'End rewards: ', reward)
    #    print( 'Bets: ', current_bets)
    #    print()
    # applying end rewards to main collector
    collector.end_rewards(reward, current_bets)

    if debug:
        display_end_game(game.money, reward)
        input_stop()

    return collector, current_bets, reward, game_loss

# ------------- TRAINING MODELS ------------

def train_models(sess, bet_maker, game_decision, collector):
    # normalizing rewards
    collector.discount_rewards()

    # calculating mean gradient
    bet_feed_dict = bet_maker.calculate_mean_gradientv2(collector.bet_gradients,
                                                        collector.normalized_bet_rewards)
    bot_feed_dict = game_decision.calculate_mean_gradient(collector.game_gradients,
                                                          collector.normalized_game_rewards)

    # running trainer to apply gradients
    sess.run(game_decision.trainer, feed_dict=bot_feed_dict)
    sess.run(bet_maker.trainer, feed_dict=bet_feed_dict)

def insert_game_results(collector, current_money, game_reward, current_bet, epoch, game_index, play_index):
    collector.insert_row({'Epoch': epoch,
                          'Current money': current_money,
                          'Game index': game_index,
                          'Game reward': np.sum(game_reward),
                          'Play index': play_index,
                          'Game bet': current_bet})
    return collector

def train_new_model(base_folder='Trained models/', model_name='model'):
    tf.reset_default_graph()
    game_spec, player_spec, betting_spec = create_model_parameters()
    display_specification(game_spec, player_spec, betting_spec)

    game_decision, bet_maker = create_models(game_spec, player_spec, betting_spec)
    game = create_game(game_spec)

    model_collector = rgc.ModelAnalysis(
        ['Epoch', 'Game bet', 'Game index', 'Current money', 'Game reward', 'Play index'])

    collector = rgc.GBCollector(game_discount_rate=player_spec['discount rate'],
                                bet_discount_rate=betting_spec['discount rate'])

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run( init)

        for epoch in range( game_spec[ 'n epochs']):
            if epoch % 5 == 0:
                print('\tepoch: ', epoch)

            collector.reset()
            game_losses = []
            for game_index in range( game_spec['n games per epoch']):
                game.reset_iteration()

                for play_index in range( game_spec['n max bets']):
                    if game.money <= 0:
                        break

                    collector, current_bet, current_rewards, game_loss = black_jack_full_game(
                        sess, game, collector, game_decision, bet_maker, game_spec, player_spec, game_index=play_index,
                        debug=(epoch >= 2) and (play_index > 30)
                    )
                    game_losses.extend( game_loss)

                    model_collector = insert_game_results(
                        model_collector, game.money, current_rewards, current_bet, epoch, game_index, play_index
                    )

            print( 'Game decision loss: ', np.mean( np.array( game_losses)))
            train_models(sess, bet_maker, game_decision, collector)

        make_directory(base_folder)

        save_specifications(game_spec, player_spec, betting_spec, base_folder)
        saver.save(sess, base_folder + model_name + '.ckpt')
        model_collector.save_analysis( base_folder)

for index in range(30):
    print( 'training model with index: ', index)
    train_new_model()