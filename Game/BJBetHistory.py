import numpy as np

class BJBetHistory:

    def __init__(self, num_slots, history_length):
        self.num_slots = num_slots
        self.history_length = history_length
        self.reset()

    def add_rewards(self, rewards):
        self.history = np.append(self.history, [rewards], axis=0)

        if len( self.history) > self.history_length:
            self.history = self.history[1:]

    def get_history(self):
        return self.history

    def get_normalizer_history(self, normalizator):
        return self.history / normalizator

    def reset(self):
        self.history = np.zeros((self.history_length, self.num_slots))
