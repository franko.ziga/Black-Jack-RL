import numpy as np
import math
import Game.BJCards as bjc

class BJCardHistory:

    def __init__(self, n_decks, max_save=10):
        self.n_decks = n_decks
        self.history_log, self.max_history_log = np.array([]), max_save
        self.reset()

    def current_values(self, values):
        self.history_log = np.append(self.history_log, [values], axis=0)

        if len( self.history_log) > self.max_history_log:
            self.history_log = self.history_log[1:]

    def reset(self):
        self.history = np.array([])
        self.history_log = np.zeros((self.max_history_log, 1))

    def add_card_value(self, card_value):
        '''
        :param card_value: str of card value
        add card value into history array
        '''
        self.history = np.append(self.history, card_value)

    def get_history_log(self):
        return self.history_log

    def calculate_history_observation(self):
        '''
        :return: array of value propabilities ['2': 0.1, '3': 0.05,... 'A': 0.14]
        '''
        # group by each card value and count them
        if len( self.history) != 0:
            values = map( lambda x: self.history[self.history == x].shape[0], bjc.Card.viableValues())
        else:
            values = np.array( [1.] * 13)
            return values
        values = np.fromiter(values, dtype=np.float)

        # all possible cards
        possible_cards = self.n_decks * 4
        current_deck_length = math.ceil( (self.n_decks * 52 - len( self.history))/52)

        # calculating probability of each card value
        values = (possible_cards - values) / current_deck_length
        values = (values - 0.5) * 2
        #values = values / np.mean( values)

        self.current_values( values)

    def calculate_mit_history_observation(self):
        if len( self.history) == 0:
            return np.array( [0])

        value_fvect = np.vectorize( self.getValue)
        values = value_fvect( self.history)

        values[ values < 7] = 1
        values[ (values > 6) * (values < 10)] = 0
        values[ values > 9] = -1

        current_deck_length = int( (self.n_decks * 52 - len( self.history))/52)
        current_deck_length = 1 if current_deck_length < 1 else current_deck_length

        values = [np.sum( values) / current_deck_length]
        self.current_values( values)

    def getValue(self, value):
        value_map = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'J': 10, 'Q': 10,
                     'K': 10, 'A': 11}
        return value_map[value]

    def get(self):
        return self.history
