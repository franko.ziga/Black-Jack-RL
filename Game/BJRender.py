import pygame
import math

class BJRender:

    card_width, card_height = 45, 90
    slot_distance = 450

    Background  = ( 18,  94,  15)
    White       = (255, 255, 255)
    Slot_color  = (255, 109,   0)

    def __init__(self, size):
        '''
        :param size: tuple of width and heigh
        '''
        pygame.init()
        self.width, self.height = size
        self.clock = pygame.time.Clock() # used for frames per second
        self.make_display()

        self.font = pygame.font.Font('freesansbold.ttf', 20)
        self.action = 5
        self.actions = ['Stand', 'Hit', 'Double', 'Split', 'Surrender', '']

    def reset_action(self):
        self.action = 5

    def update(self):
        self.user_event()
        self.clock.tick( 60)

    def end(self):
        pygame.quit()

    def make_display(self):
        self.display = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Black Jack")

    def user_event(self):
        pygame.event.get()

    def draw(self, dealer, slots, money, current_slot_index):
        self.draw_background()
        self.text_object('Money: {}'.format(money), (120, 30), self.White)
        self.text_object(self.actions[ self.action], (self.width/2, 250), self.White)

        slots = slots[ slots != None]
        num_slots = len( slots)
        start_x, start_y = self.width / 2, -40
        angle = 150 / (num_slots + 1)

        self.draw_slot_cards( dealer.get_display(full_display=True), (self.width/2, self.card_height), self.White)
        self.text_object( 'DV: {}'.format(dealer.getDeckValue()), (self.width/2, self.card_height + 80), self.White)

        for slot_pos in range(1, num_slots + 1):
            x = int(start_x + math.cos(math.radians(angle * slot_pos + 15)) * self.slot_distance)
            y = int(start_y + math.sin(math.radians(angle * slot_pos + 15)) * self.slot_distance)

            self.draw_slot(
                x,
                y,
                slots[ slot_pos-1],
                self.Slot_color if slot_pos - 1 == current_slot_index else self.White
            )

        pygame.display.flip()

    def draw_slot(self, x, y, slot, color):
        slot_bet = str( 0 if slot == None else slot.get_bet())
        slot_cards = [] if slot == None else slot.get_display()

        self.draw_slot_bet( slot_bet, (x, y), color)
        self.draw_slot_cards( slot_cards, (x, y + 50 + self.card_width), color)
        self.text_object('DV: {}'.format( slot.deck.getDeckValue()), (x, y + 170), color)

    def draw_background(self):
        self.display.fill( self.Background) # filling display with background color
        pygame.draw.ellipse(self.display, self.White, (0, -self.height/2, self.width, self.height), 5)    # dealers ellipse

    def draw_slot_bet(self, bet, position, color):
        # plotting circle around bet
        pygame.draw.circle(self.display, color, position, 30, 3)

        # text bet
        self.text_object(bet, position, color)

    def draw_slot_cards(self, card_display, position, color):
        if len( card_display) == 0:
            return

        current_x, current_y = position
        num_cards = len( card_display)
        step_width = self.card_width + self.card_width/10
        start_x = current_x - num_cards/2 * step_width

        for card_index in range( num_cards):
            self.draw_card(card_display[ card_index], (start_x + step_width * card_index + step_width/2, current_y), color)

    def draw_card(self, card, position, color):
        text_x, text_y = position
        start_x, start_y, = text_x - self.card_width/2, text_y - self.card_height/2

        # display shape of card
        pygame.draw.rect(self.display, color, (start_x, start_y, self.card_width, self.card_height), 3)

        # displaying text of card
        self.text_object(card, position, color)

    def text_object(self, text, position, color):
        textSurface = self.font.render(text, True, color)
        text_surface, text_rect = textSurface, textSurface.get_rect()
        text_rect.center = position
        self.display.blit(text_surface, text_rect)

'''
render = BJRender((800, 600))

slots = [bjp.Slot('slot 1'), bjp.Slot('slot 2'), bjp.Slot('slot 3')]

slots[0].setBet( 4)
slots[1].setBet( 6)
slots[2].setBet( 1)

dealer = bjp.Dealer()
dealer_deck = bjc.Deck( 8)

for i in range(3):
    dealer.addCard( dealer_deck.drawCard())

for slot in slots:
    for i in range( 4):
        slot.addCard( dealer_deck.drawCard())

while True:
    render.draw( dealer, slots)
    render.user_event()
    render.update()

render.end()
'''