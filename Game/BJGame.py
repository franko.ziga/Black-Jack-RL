import Game.BJCards as bjc
import Game.BJPeople as bjp
import Game.BJRender as bjr
import Game.BJBetHistory as bjbh
import Game.BJCardHistory as bjch

import numpy as np

class BlackJack:

    num_slots = 0
    slot_index = 0
    money = 0

    # we have bots for dealer and player
    dealer = None
    dealer_deck = None

    # containing slot as each slot is a Person
    slots = np.array( [None])

    create_slots_vec = None

    card_history = None
    bet_history = None

    # -------------- INITIALIZATION -----------
    def make(self, num_decks, start_money=300, normalizator=10, num_slots=3, max_history=20, render=False):
        self.num_decks = num_decks
        self.num_slots = num_slots
        self.max_history = max_history
        self.starting_money = start_money
        self.money = start_money
        self.normalizator = normalizator

        self.dealer = bjp.Dealer()

        self.create_slots_vec = np.vectorize( self.create_slot)

        self.card_history = bjch.BJCardHistory( self.num_decks, max_save=max_history)
        self.bet_history = bjbh.BJBetHistory( num_slots, max_history)

        self.rendering = render

        if self.rendering:
            self.render = bjr.BJRender( (800, 600))

    # ------------- RESET METHODS ----------
    # resetting iteration used when creating new game
    def reset_iteration (self):
        self.dealer_deck = bjc.Deck( self.num_decks)
        self.money = self.starting_money
        self.card_history.reset()
        self.bet_history.reset()
        self.money_log = [0] * self.max_history

    # clear decks, set slot bets to 0, slot_index to 0
    def reset (self):
        self.dealer.clearDeck()
        self.slots = np.array( self.create_slots_vec( list( range( self.num_slots))))
        self.slots_wrong_move = [False] * self.num_slots
        self.slot_index = -1
        self.grahical_display()

        if self.rendering: self.render.reset_action()

        self.card_history.calculate_mit_history_observation()
        self.current_money()
        observation = {}
        observation[ 'Current money'] = self.money_log/self.starting_money
        observation[ 'Bet history'] = self.bet_history.get_normalizer_history( self.normalizator)
        observation[ 'Card history'] = self.card_history.get_history_log()
        return observation

    # ---------- MONEY LOG ----------
    def current_money(self):
        self.money_log = np.append( self.money_log, [self.money])
        self.money_log = self.money_log[1:]

    # ---------- INSERTING BETS --------------
    # setting slots bets and continue using only slots that made their bet
    # now this should work fine #hopefully! :D
    def set_bets (self, slots_bet):
        slots_bet = np.array( slots_bet)
        # repairing beats if necessary
        slots_bet = self.repare_bets( slots_bet)

        # assigning bets to slots
        [slot.set_bet(bet) for slot, bet in zip(self.slots, slots_bet)]
        # keeping only slots which we will play with
        self.slots[ slots_bet == 0.0] = None
        # if there is no slots to play with send signal to break game
        return np.sum( self.slots != None) == 0

    def repare_bets (self, slot_bets):
        mean_difference = np.sum( slot_bets) - self.money
        if mean_difference <= 0:
            return slot_bets

        for index in range( len( slot_bets)):
            if slot_bets[ index] > 0:
                slot_bets[ index] -= mean_difference
                mean_difference = 0
            if slot_bets[ index] < 0:
                mean_difference = slot_bets[ index] * -1
                slot_bets[ index] = 0
            if mean_difference == 0:
                break

        return slot_bets

    # -------------- DEALING CARDS -------------
    # drawing card
    def get_card(self, person, to_history=True):
        if person == None:
            return

        if self.dealer_deck.cardsSize() <= 10:
            self.dealer_deck = bjc.Deck( self.num_decks)
            self.card_history.reset()

        card = self.dealer_deck.drawCard()
        person.addCard( card)

        if to_history:
            self.card_history.add_card_value( card.value)

    # dealing cards to all people in the game
    def deal_cards (self):
        [self.get_card( slot) for slot in self.slots]
        self.get_card( self.dealer)
        [self.get_card( slot) for slot in self.slots]

        self.slot_index = self.find_next_index()

    def dealer_playing(self):
        #if self.game_natural:
        #    if self.dealer.encoded_deck()[0] < 9/13:
        #        return
        while self.dealer.action( None) == 1:
            self.get_card( self.dealer)

            #if self.game_natural:
            #    if not self._if_natural( self.dealer):
            #        return

    # ------------ PLAYING GAME -----------
    def game_step(self, action):
        '''
        :param action: action you want to preform
        :return: True if action is viable else False
        '''
        slot = self.slots[ self.slot_index]
        if self.rendering: self.render.action = int(action)

        if action == 0:     # stand
            self.slot_index = self.find_next_index()

        elif action == 1:   # hit
            self.get_card( slot)
            if slot.deck.getDeckValue() > 21:
                self.slot_index = self.find_next_index()

        elif action == 2:   # double
            slot.set_bet( slot.get_bet() * 2)

            if slot.getDeckSize() == 2:
                self.get_card( slot)
                self.slot_index = self.find_next_index()
            else:
                #print( 'wrong move Double, hand', slot.encoded_deck())
                self.slots_wrong_move[ self.slot_index] = True
                self.slot_index = self.find_next_index()
                return False

        elif action == 4:   # split
            if self.check_split( slot):
                self.slots_wrong_move.insert(self.slot_index, False)
                new_slot = bjp.Slot( "sub")
                new_slot.set_bet( slot.get_bet())

                new_slot.addCard( slot.pop_card())
                self.get_card( slot)
                self.get_card( new_slot)

                self.slots = np.insert(self.slots, self.slot_index + 1, new_slot)
            else:
                #print( 'wrong move Split, hand', slot.encoded_deck())
                self.slots_wrong_move[ self.slot_index] = True
                self.slot_index = self.find_next_index()
                return False
        elif action == 3: # surrender
            if self._check_surrender( slot):
                slot.set_bet( slot.get_bet()/2)
                slot.surrender = True
            else:
                #print( 'wrong move Surrender, hand', slot.encoded_deck())
                self.slots_wrong_move[ self.slot_index] = True
                self.slot_index = self.find_next_index()
                return False
            self.slot_index = self.find_next_index()

        #if np.sum( self.slots_wrong_move) > 0:
            #print('wrong move!')
        return True

    def check_split(self, slot):
        deck_values = slot.encoded_deck()
        return (len( deck_values) == 2) and (deck_values[0] == deck_values[1])

    def _check_surrender(self, slot):
        return len( slot.encoded_deck()) == 2

    # TODO PREGLEJ ZA NAPAKE!
    def observe_game(self):
        '''
        :return observation, reward, done
        '''
        observation = {}
        observation[ 'Slot index'] = self.slot_index

        if self.slot_index == -1:
            self.dealer_playing()
            dealer_value = self.dealer.getDeckValue()

            rewards = np.array( [self.result( slot, dealer_value, wrong_move)
                                 for slot, wrong_move in zip( self.slots, self.slots_wrong_move)])

            self.grahical_display()
            observation[ 'Dealer'] = self.dealer.getCardValues(full_display=True)
            compressed_rewards = self.compress_rewards( rewards)

            self.money += np.sum( compressed_rewards)
            self.bet_history.add_rewards( compressed_rewards)
            return observation, rewards, True


        self.card_history.calculate_mit_history_observation()
        self.grahical_display()
        current_slot = self.slots[ self.slot_index]
        observation[ 'Current money'] = self.money/self.normalizator
        observation[ 'Current bet'] = current_slot.current_bet/self.normalizator
        observation[ 'Current slot'] = current_slot.encoded_deck()/13
        observation[ 'Deck value'] = current_slot.getDeckValue()/17
        observation[ 'Dealer'] = self.dealer.encoded_deck()/13
        observation[ 'Card history'] = self.card_history.get_history_log()
        return observation, 0, False

    # --------- RENDERING -----------
    def grahical_display(self):
        if self.rendering:
            self.render.draw(self.dealer, self.slots, self.money, self.slot_index)
            self.render.update()

    def compress_rewards(self, rewards):
        compress_rewards_indexes = np.where([1 if slot != None and slot.name == 'sub' else 0
                                             for slot in self.slots])[0]

        for index in range( compress_rewards_indexes.shape[0]):
            true_index = compress_rewards_indexes[ index]
            first, add, last = rewards[:true_index], rewards[true_index], rewards[true_index + 1:]
            first[true_index - 1] += add
            compress_rewards_indexes = compress_rewards_indexes - 1
            rewards = np.append(first, last)
        return rewards

    def find_next_index(self):
        for slot_index in range( self.slot_index + 1, len( self.slots)):
            if self.slots[ slot_index] != None:
                return slot_index
        return -1

    # creating slot
    def create_slot (self, name):
        return bjp.Slot(name)

    def result(self, slot, dealer_deck_value, wrong_move):
        if slot == None:
            return 0
        if wrong_move:
            slot.set_bet( -slot.current_bet*3)
            return slot.current_bet

        deck_value = slot.getDeckValue()
        if slot.surrender:
            reward = -slot.current_bet
        elif (dealer_deck_value <= 21) and (dealer_deck_value == deck_value):
            reward = 0
        elif self._if_natural( slot):
            reward = slot.current_bet * 1.5
        elif deck_value > 21:
            reward = -slot.current_bet
        elif dealer_deck_value > 21:
            reward = slot.current_bet
        elif dealer_deck_value > deck_value:
            reward = -slot.current_bet
        else:
            reward = slot.current_bet

        slot.set_bet( reward)
        return reward

    def display(self):
        return '{}\n{}'.format(self.dealer.display(full_display=True), '\n'.join([slot.display() for slot in self.slots if slot != None]))

    def _if_natural(self, slot):
        hand = slot.encoded_deck()
        if len( hand) > 2: return False
        (ace, second) = (hand[0], hand[1]) if hand[0] == 13 else (hand[1], hand[0])
        if ace != 13: return False
        natural = (second != 13.) and (second > 8)
        return natural
