import numpy as np
import itertools

class Card:

    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

        self.encoder = {card: index + 1 for index, card in enumerate( Card.viableValues())}
        self.value_map = {'2': 2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, '10':10, 'J':10, 'Q':10, 'K':10, 'A':11}

    def display (self):
        return "" + self.value + self.suit

    def getCardEncoder(self):
        return self.encoder[ self.value]

    def getValue (self):
        return self.value_map[ self.value]

    def viableSuits ():
        return np.array(['D', 'C', 'S', 'H'])

    def viableValues ():
        return np.array(['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'])

class Deck:
    def __init__(self, n_decks):
        if n_decks == 0:
            self.clear()
            return

        self.cardHolder = [Card(suit, value) for suit, value in
                           itertools.product(Card.viableSuits(), Card.viableValues())] * n_decks
        self.cardHolder = np.array( self.cardHolder)
        np.random.shuffle( self.cardHolder)

    def addCard (self, card):
        self.cardHolder = np.append(self.cardHolder, card)

    def cardsSize (self):
        return self.cardHolder.size

    def clear (self):
        self.cardHolder = np.array([])

    def drawCard (self):
        if self.cardsSize() <= 0:
            return None
        card, self.cardHolder = self.cardHolder[ 0], self.cardHolder[1:]
        return card

    def popCard(self):
        if self.cardsSize() <= 0:
            return None
        card, self.cardHolder = self.cardHolder[-1], self.cardHolder[:-1]
        return card

    def getDeckValue (self):
        if self.cardsSize() <= 0:
            return 0
        valuefun = np.vectorize( Card.getValue)

        values = valuefun( self.cardHolder)
        total_value = np.sum( values)
        if total_value <= 21:
            return total_value

        tol_values = values[ values != 11]
        e_values = values[ values == 11]

        total_value = np.sum(tol_values)

        for index in range( len( e_values)):
            if total_value + 11 <= 22 - len( e_values) + index:
                total_value += 11
            else:
                total_value += 1
        return total_value

    def encodedDeck(self):
        encoded = [card.getCardEncoder() for card in self.cardHolder]
        return np.array( encoded)

    def getValues(self):
        vect_value = np.vectorize( Card.getValue)
        return vect_value( self.cardHolder)

    def display (self):
        if self.cardsSize() <= 0:
            return ""
        display_card = np.vectorize( Card.display)
        return ", ".join( display_card( self.cardHolder))

    def get_cards_display(self):
        if self.cardsSize() == 0:
            return np.array([])
        vec_display = np.vectorize( Card.display) # vectorization of display funciton
        return vec_display( self.cardHolder)

'''

player_deck = Deck(0)
test = Deck(2)


player_deck.addCard( test.drawCard())
player_deck.addCard( test.drawCard())
player_deck.addCard( test.drawCard())
player_deck.addCard( test.drawCard())

print( player_deck.display())
print( player_deck.getDeckValue())
'''