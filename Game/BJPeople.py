import Game.BJCards as bjc
import numpy as np

class Person:
    def __init__(self):
        self.deck = bjc.Deck(0)

    def addCard (self, card):
        self.deck.addCard( card)

    def getDeckValue (self):
        return self.deck.getDeckValue()

    def clearDeck (self):
        self.deck.clear()

    def getCardValues(self):
        return self.deck.getValues()

    def getDeckSize (self):
        return self.deck.cardsSize()

    def encoded_deck(self):
        return self.deck.encodedDeck()

    def display(self):
        return 'Deck: {}\nValue of deck: {}'.format(self.deck.display(), self.getDeckValue())

    def get_display(self):
        return self.deck.get_cards_display()

    def action(self, obs):
        return 0

class Dealer( Person):
    def __init__(self):
        Person.__init__(self)

    def action(self, obs):
        value = self.getDeckValue()
        if value < 17:
            return 1
        if self._soft_17(value):
            return 1
        return 0

    def getCardValues(self, full_display=False):
        cards = self.deck.getValues()
        return cards if full_display else cards[:1]

    def get_display(self, full_display=False):
        cards = self.deck.get_cards_display()
        return cards if full_display else cards[:1]

    def display(self, full_display=False):
        return 'Dealer deck: {}\n\tValue of deck: {}'.format(self.deck.cardHolder[0].display() if not full_display else self.deck.display(),
                                                   self.deck.cardHolder[0].getValue() if not full_display else self.deck.getDeckValue())

    def _soft_17(self, value):
        hand = self.encoded_deck()
        if (len( hand) != 2) or (value != 17): return False
        native = (hand[0] == 13) or (hand[1] == 13)
        return native


class Player( Person):
    def __init__(self, name):
        Person.__init__(self)
        self.name = name

    def action(self, obs):
        return int( input("Action: "))

    def action_bet(self, obs):
        return int( input( "Bet: "))

class Slot( Person):
    def __init__(self, slot_name):
        Person.__init__(self)
        self.name = slot_name
        self.current_bet = 0
        self.surrender = False

    def pop_card(self):
        return self.deck.popCard()

    def set_bet(self, bet):
        self.current_bet = bet

    def get_bet(self):
        return self.current_bet

    def display(self):
        return "Slot {}:\nDeck: {}\n\tValue of deck: {}".format( self.name, self.deck.display(), self.deck.getDeckValue())

class BasicPlayer(Person):
    def __init__(self):
        Person.__init__(self)
        self.split_decision_tree = self._create_split_decision_tree()
        self.ace_decision_tree = self._create_ace_decision_tree()
        self.value_decision_tree = self._create_value_decision_tree()

        self.dealer_encoder = {key/13: key-1 for key in range(1, 14)}
        self.split_encoder = {key/13: key-1  for key in range(1, 14) if key not in [10, 11, 12]}
        self.split_encoder[ 1.] = 9
        self.value_index_encoder = lambda x: int(x-5)

        print( 'dealer encoder: ', self.dealer_encoder)
        print( 'split encoder: ', self.split_encoder)
        print( 'split action table: \n', self.split_decision_tree)
        print( 'ace action table: \n', self.ace_decision_tree)
        print( 'value action table: \n', self.value_decision_tree)


    def _create_split_decision_tree(self):
        # setting split
        split_decision_tree = np.ones(( 10, 13)) * 4
        # setting stand
        split_decision_tree[8, :] = 0
        split_decision_tree[7, 8:] = 0
        split_decision_tree[7, 5] = 0
        split_decision_tree[5, 8:12] = 0
        # setting double
        split_decision_tree[2, [3, 4]] = 2
        split_decision_tree[3, :8] = 2
        # setting hit
        split_decision_tree[[0, 1, 2, 4], 6:] = 1
        split_decision_tree[3, 8:] = 1
        split_decision_tree[2, :3] = 1
        split_decision_tree[[2, 4], 5] = 1
        split_decision_tree[5, [6, 7, 12]] = 1
        split_decision_tree[0, 0] = 1
        split_decision_tree[1, :2] = 1
        return split_decision_tree

    def _create_ace_decision_tree(self):
        ace_decision_tree = np.ones((8, 13))
        # setting stand
        ace_decision_tree[7, :] = 0
        ace_decision_tree[6, :4] = 0
        ace_decision_tree[6, 5:] = 0
        ace_decision_tree[5, [0, 5, 6, 12]] = 0
        # setting double
        ace_decision_tree[:6, 2:5] = 2
        ace_decision_tree[4, 0] = ace_decision_tree[6, 4] = 2
        ace_decision_tree[[4, 5], 1] = 2

        return ace_decision_tree

    def _create_value_decision_tree(self):
        value_decision_tree = np.ones((17, 13))
        # setting stand
        value_decision_tree[12:, :] = 0
        value_decision_tree[8:12, :5] = 0
        value_decision_tree[7, 2:5] = 0
        # setting double
        value_decision_tree[6, :12] = 2
        value_decision_tree[5, :8] = 2
        value_decision_tree[4, 1:5] = 2
        # setting surrender
        value_decision_tree[11, 7:] = 3
        value_decision_tree[10, 8:12] = 3
        return value_decision_tree

    def _check_split(self, hand):
        if len(hand) > 2: return False
        return (hand[0] == hand[1]) and (hand[0] * 13 not in [10, 11, 12])

    def _check_ace(self, hand):
        if len(hand) > 2: return False
        split = (hand[0] == 1.) or (hand[1] == 1.)
        if not split: return False
        second_card = self._ace_get_second_card(hand)
        return second_card * 13 < 9

    def _ace_get_second_card(self, hand):
        return hand[1] if hand[0] == 1. else hand[0]

    def action(self, obs):
        dealer_card =obs['Dealer'][0]
        hand = obs['Current slot']
        dealer_index = self.dealer_encoder[ dealer_card]
        deck_value = obs[ 'Deck value'] * 17

        if self._check_split( hand):
            card_index = self.split_encoder[ hand[0]]
            return self.split_decision_tree[card_index, dealer_index]

        elif self._check_ace( hand):
            card = self._ace_get_second_card( hand)
            card_index = self.split_encoder[ card]
            return self.ace_decision_tree[card_index, dealer_index]

        value_index = self.value_index_encoder(deck_value)
        decision = self.value_decision_tree[value_index, dealer_index]
        if ((decision == 3) and (len(hand) != 2)) or ((decision == 2) and (len(hand) != 2)):
            decision = 1
        return decision