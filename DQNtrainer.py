from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU, PReLU
from keras.optimizers import Adam

from sklearn.preprocessing import OneHotEncoder

import matplotlib.pyplot as plt

import os
import pickle
import random
import numpy as np
import pandas as pd
from collections import deque

import Game.BJGame as bj

class GameDecision:
    def __init__(self, learning_rate=0.007, lr_decay=0.000832, memory_length=1000, batch_size=32,
                 gamma=1., start_epsilon=.8, min_epsilon=0.1, epsilon_decay=0.9998,
                 clipping_value=1.7, positive_reward_addition=0.33, dropout_rate=0.1):
        self.n_inputs = 11
        self.n_actions = 5

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, lr_decay, clipping_value)
        self.memory = self._create_replay_memory( memory_length)

    def _build_netowrk(self, learning_rate, lr_decay, clipping_value):
        model = Sequential()
        model.add( Dense( 128, activation='linear', input_dim=self.n_inputs))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 64, activation='linear'))
        model.add( PReLU())
        # model.add( ELU( alpha=.5))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( self.n_actions, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=learning_rate, clipvalue=clipping_value, decay=lr_decay))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 5
        elif hand.shape[0] == 2:
            return 4
        return 2

    def _epsilon_decay_step(self):
        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        possibilities = [1, 1, 1, 1, 3][:max_action]

        if np.random.rand() < self.epsilon:
            return np.random.choice([0, 1, 2, 4, 3][:max_action], p=possibilities/np.sum(possibilities))

        model_action = self.model.predict( x)[0]
        return np.argmax( model_action)

    def train(self):
        if self.batch_size > len( self.memory):
            return 0

        mini_batch = random.sample(self.memory, self.batch_size)
        x_mini_batch, y_mini_batch = [], []

        for state, action, reward, next_state, done, _ in mini_batch:
            if done:
                target = reward
            else:
                target = reward + np.sum( [self.gamma * np.amax( self.model.predict( sub_next_state)[0])
                                           for sub_next_state in next_state])
            y = self.model.predict( state)
            y[0][action] = target

            y_mini_batch.append( y)
            x_mini_batch.append( state)

        x_mini_batch = np.array( x_mini_batch).reshape( [self.batch_size, self.n_inputs])
        y_mini_batch = np.array( y_mini_batch).reshape( [self.batch_size, self.n_actions])

        loss = self.model.fit( x_mini_batch, y_mini_batch, verbose=0)

        self._epsilon_decay_step()
        return np.mean( loss.history[ 'loss'])

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done, next_slot):
        self.memory.append( (state, action, reward, [next_state], done, next_slot))

    def repare_rewards(self, end_rewards):
        last_index = len( end_rewards)-1
        memory_index = len( self.memory)-1

        end_rewards = np.array( end_rewards, dtype=np.float32)
        if self.positive_reward_addition < 0:
            end_rewards[ end_rewards < 0.] += self.positive_reward_addition
        else:
            end_rewards[ end_rewards > 0.] += self.positive_reward_addition

        sub_slot_state = None
        while last_index >= 0:
            state, action, reward, next_state, done, new_slot = self.memory[ memory_index]
            if action == 3:
                self.memory[ memory_index] = (state, action, reward, [next_state[0], sub_slot_state], done, new_slot)
            if new_slot:
                sub_slot_state = state
            if done:
                reward = end_rewards[ last_index]
                self.memory[ memory_index] = (state, action, reward, next_state, done, new_slot)
                last_index -= 1
            memory_index -= 1


def convert_inputs( state, n_card_slots=8):
    x = []
    x.append( state['Current bet']) # 1 position
    x.extend( state['Dealer']) # 1 position
    x.append( state['Deck value'])

    n_current_slot = len( state['Current slot'])
    if n_current_slot <= n_card_slots:
        difference = [0] * (n_card_slots - n_current_slot)
        slot_cards = np.concatenate([state['Current slot'], difference])
    else:
        slot_cards = state['Current slot'][:n_card_slots]

    x.extend( slot_cards) # n card slots
    x = np.array( x)
    return np.reshape(x, [1, len(x)]) # all of them forms size of 24 inputs, currently used only 23!

def plot_end_money( money, window=1, min_periods=1):
    plt.figure()

    data = pd.Series( money)

    mean = data.rolling(window, min_periods=min_periods).mean()
    mn = data.rolling(window, min_periods=min_periods).min()
    mx = data.rolling(window, min_periods=min_periods).max()

    epochs = range( len( money))
    plt.title( 'End money through iterations')
    plt.ylabel('Money')
    plt.xlabel('Iteration')
    plt.plot(epochs, money, label='true end rewards')
    plt.plot(epochs, mn, label='rolling min')
    plt.plot(epochs, mx, label='rolling max')
    plt.plot(epochs, mean, label='rolling mean')

    plt.show()

def betting_step(game):
    # resetting all cards and bets
    obs = game.reset()

    if obs['Current money'][-1] <= 0:
        return

    # bets = player.action_bet( obs)
    bets = [1, 1, 1]
    _ = game.set_bets(bets)

def game_decision_step(game, player):
    game.deal_cards()

    state, reward, done = game.observe_game()
    slot_index = state['Slot index']
    hand = state['Current slot']
    state = convert_inputs(state)

    actions=[]

    while True:
        action = player.action(state, hand)
        actions.append(action)
        game.game_step(action)

        next_state, reward, done = game.observe_game()

        if done:
            player.remember(state, action, 0, None, True, (next_state['Slot index'] != slot_index))
            break
        elif next_state['Slot index'] != slot_index:
            player.remember(state, action, 0, None, True, True)
            hand = next_state['Current slot']
            slot_index = next_state['Slot index']
            next_state = convert_inputs(next_state)
        else:
            hand = next_state['Current slot']
            next_state = convert_inputs(next_state)
            player.remember(state, action, 0, next_state, False, False)

        state = next_state

    player.repare_rewards(reward)
    return actions

def epoch_step(game, player, n_epochs=150, n_max_games=831, sub_print=True, end_plot=True):
    end_money = []
    for epoch in range(n_epochs):
        game.reset_iteration()
        actions = []

        for game_index in range(n_max_games):
            betting_step(game)
            sub_actions = game_decision_step(game, player)
            actions.extend( sub_actions)

            loss = player.train()
            if (game_index % 250 == 0) and sub_print:
                print('game index: {}, epsilon: {}, loss: {}'.format(game_index, player.epsilon, loss))

        end_money.append(game.money)
        if sub_print:
            print('epoch: {}, end game index: {}, end game money: {}'.format(epoch, game_index, game.money))
            print('action distribution stand: {}, hit: {}, double: {}, split: {}, surrender: {}'.format(
                actions.count(0),
                actions.count(1),
                actions.count(2),
                actions.count(3),
                actions.count(4)
            ))
            print()

    if end_plot:
        plot_end_money(end_money, window=int(n_epochs / 10))

    return player, end_money

# FOLDER MANAGEMENT
def make_directory(folder_path):
    if not os.path.exists( folder_path):
        os.makedirs( folder_path, exist_ok=True)

def next_directory(folder_base):
    models = [int(model_name.split(' ')[1]) for model_name in os.listdir( folder_base)
              if os.path.isdir(folder_base + model_name)]
    if len( models) == 0:
        model_name = 'Model 0'
    else:
        last_number = int( sorted( models)[-1]) + 1
        model_name = 'Model ' + str( last_number)
    path = folder_base + model_name + '/'
    make_directory(path)
    return path

def prepare_folder(folder_path):
    make_directory( folder_path)
    new_folder_path = next_directory( folder_path)
    return new_folder_path

def save_model_parameters(parameters, end_money, folder):
    with open(folder + 'gd.parameters.pkl', 'wb') as file:
        pickle.dump(parameters, file)
    with open(folder + 'traning results.pkl', 'wb') as file:
        pickle.dump(end_money, file)

# CREATING MODEL PARAMETERS AND MODEL FROM IT
def create_random_game_decision_parameters():
    model_parameters = {}
    model_parameters[ 'learning rate'] = 10 ** (-np.random.uniform(1.3, 3))
    model_parameters[ 'learning rate decay'] = 10 ** (-np.random.uniform(3, 5))
    model_parameters[ 'gamma'] = np.random.uniform(0.99000, 0.99999)
    model_parameters[ 'memory length'] = int(np.random.uniform(4000, 8000))
    model_parameters[ 'batch size'] = np.random.choice([8, 16, 32, 40, 50])
    model_parameters[ 'epsilon decay'] = 1 - 10 ** (-np.random.uniform(3.9, 4.3))
    model_parameters[ 'clipping value'] = np.random.uniform(0.5, 2)
    model_parameters[ 'positive additional reward'] = np.random.uniform(-0.9, 0.9)
    return model_parameters

def create_model(parameters):
    model = GameDecision(
        learning_rate=parameters['learning rate'],
        lr_decay=parameters['learning rate decay'],
        gamma=parameters['gamma'],
        memory_length=parameters['memory length'],
        batch_size=parameters['batch size'],
        epsilon_decay=parameters['epsilon decay'],
        clipping_value=parameters['clipping value'],
        positive_reward_addition=parameters['positive additional reward']
    )
    return model

def display_secifications(parameters):
    print('Game decision parameters')
    for key in parameters.keys():
        print('{} -> {}'.format(key, parameters[key]))

def hyperparameter_search(n_models, n_slots=3, n_decks=8, n_epochs=50, n_games=831, start_money=831, base_folder='dqn model/models/'):
    game = bj.BlackJack()
    game.make(n_decks, start_money=start_money, num_slots=n_slots)

    for model_index in range(n_models):
        print('model ', model_index, ' is training...')
        model_parameters = create_random_game_decision_parameters()
        display_secifications( model_parameters)
        player = create_model( model_parameters)
        player, end_money = epoch_step(game, player, n_epochs=n_epochs, n_max_games=n_games, sub_print=True, end_plot=False)

        model_folder = prepare_folder( base_folder)
        player.save_model( model_folder+'game-decision.h5')
        save_model_parameters(model_parameters, end_money, model_folder)

if __name__ == '__main__':

    # hyperparameter_search(50, n_decks=6, n_epochs=50)

    n_slots = 3
    n_decks = 8
    n_epochs = 150
    n_max_games = 1000
    money = n_max_games

    game = bj.BlackJack()
    game.make( n_decks, start_money=money, num_slots=n_slots, normalizator=n_max_games)
    # resetting player, dealer and deck
    player = GameDecision()
    player, end_money = epoch_step(game, player, n_epochs=n_epochs, n_max_games=n_max_games, end_plot=True)
    player.save_model('game-decision.h5')


