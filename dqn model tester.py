
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, ELU
from keras.optimizers import Adam

import time
import random
import numpy as np
from collections import deque

import Game.BJGame as bj

class DQN:
    def __init__(self, learning_rate=0.005, memory_length=16384, batch_size=32,
                 gamma=0.99, start_epsilon=.6, min_epsilon=0.1, epsilon_decay=0.9997,
                 clipping_value=1., positive_reward_addition=0.1, dropout_rate=0.2):
        self.n_actions = 4

        self.epsilon = start_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.positive_reward_addition = positive_reward_addition
        self.dropout_rate = dropout_rate
        self.batch_size = batch_size
        self.gamma = gamma

        self.model = self._build_netowrk( learning_rate, clipping_value)
        self.memory = self._create_replay_memory( memory_length)

    def _build_netowrk(self, learning_rate, clipping_value):
        model = Sequential()
        model.add( Dense( 256, activation='linear', input_dim=11))
        model.add( ELU( alpha=.2))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 128, activation='linear'))
        model.add( ELU( alpha=.2))
        # model.add( Dropout( self.dropout_rate))
        model.add( Dense( 4, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=learning_rate, clipvalue=clipping_value, decay=1e-4))
        return model

    def _create_replay_memory(self, max_length):
        # deque allows us inserting items into memory
        # when memory is at his max range old items will be thrown away by rule FIFO
        memory = deque( maxlen=max_length)
        return memory

    def _max_action(self, hand):
        if (hand.shape[0] == 2) and (hand[0] == hand[1]):
            return 4
        elif hand.shape[0] == 2:
            return 3
        return 2

    def action(self, x, hand):
        # choosing only available action randomly with epsilon percentage
        # else choose action from model
        max_action = self._max_action( hand)
        if np.random.rand() < self.epsilon:
            return np.random.randint(0, max_action, 1)[0]

        model_action = self.model.predict( x)[0]
        return np.argmax( model_action)

    def train(self):
        if self.batch_size > len( self.memory):
            return

        mini_batch = random.sample(self.memory, self.batch_size)
        loss = 0

        # rebuild this so you are making an s
        for state, action, reward, next_state, done, _ in mini_batch:
            if done:
                target = reward
            else:
                target = reward + np.sum( [self.gamma * np.amax( self.model.predict( sub_next_state)[0])
                                           for sub_next_state in next_state])

            target_f = self.model.predict( state)
            target_f[0][action] = target
            itt_loss = self.model.fit( state, target_f, verbose=0)
            loss += np.mean( itt_loss.history['loss'])

        # epsilon decay
        if self.epsilon > self.min_epsilon:
            self.epsilon *= self.epsilon_decay
        return loss / self.batch_size

    def save_model(self, filepath):
        self.model.save( filepath)

    def load_model(self, filepath):
        self.model = load_model( filepath)

    def remember(self, state, action, reward, next_state, done, next_slot):
        self.memory.append( (state, action, reward, [next_state], done, next_slot))

    def repare_rewards(self, end_rewards):
        last_index = len( end_rewards)-1
        memory_index = len( self.memory)-1

        end_rewards = np.array( end_rewards, dtype=np.float32)
        end_rewards[ end_rewards > 0.] += self.positive_reward_addition

        sub_slot_state = None
        while last_index >= 0:
            state, action, reward, next_state, done, new_slot = self.memory[ memory_index]

            if action == 3:
                self.memory[ memory_index] = (state, action, reward, [next_state[0], sub_slot_state], done, new_slot)
            if new_slot:
                sub_slot_state = state
            if done:
                reward = end_rewards[ last_index]
                self.memory[ memory_index] = (state, action, reward, next_state, done, new_slot)
                last_index -= 1

            memory_index -= 1


def convert_inputs( state, n_card_slots=8):
    x = []
    x.append( state['Current bet']) # 1 position
    x.extend( state['Dealer']) # 1 position
    x.append( state['Deck value'])

    n_current_slot = len( state['Current slot'])
    if n_current_slot <= n_card_slots:
        difference = [0] * (n_card_slots - n_current_slot)
        slot_cards = np.concatenate([state['Current slot'], difference])
    else:
        slot_cards = state['Current slot'][:n_card_slots]

    x.extend( slot_cards) # n card slots
    x = np.array( x)
    return np.reshape(x, [1, len(x)]) # all of them forms size of 24 inputs, currently used only 23!

if __name__ == '__main__':

    n_slots = 3
    n_decks = 6
    n_epochs = 30
    n_max_games = 1000 #n_decks * 52 * 2
    money = n_max_games

    game = bj.BlackJack()
    game.make( n_decks, start_money=money, num_slots=n_slots, normalizator=n_max_games, render=False)
    # resetting player, dealer and deck
    player = DQN(start_epsilon=0.)
    player.load_model('dqn model/game-decision.h5')

    end_money = []
    game.reset_iteration()

    for game_index in range( n_max_games):
        # resetting all cards and bets
        obs = game.reset()

        if obs['Current money'][-1] <= 0:
            break

        # bets = player.action_bet( obs)
        bets = [1, 1, 1]
        done = game.set_bets( bets)
        if done:
            break

        game.deal_cards ()

        state, reward, done = game.observe_game()
        slot_index = state[ 'Slot index']
        hand = state['Current slot']
        state = convert_inputs(state)

        while True:
            action = player.action( state, hand)
            game.game_step( action)

            next_state, reward, done = game.observe_game()

            # time.sleep( 2)
            if done:
                player.remember(state, action, 0, None, True, (next_state['Slot index'] != slot_index))
                break
            elif next_state['Slot index'] != slot_index:
                slot_index = next_state['Slot index']

            hand = next_state['Current slot']
            next_state = convert_inputs(next_state)

            state = next_state

